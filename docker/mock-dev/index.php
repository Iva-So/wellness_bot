<?php


function matchData(int $argc, array $server): array
{
    $data_Ok_True__Result_emptyArray = ([
        'ok' => true,
        'result' => [],
    ]);
    if (0 === $argc) {
        return $data_Ok_True__Result_emptyArray;
    }

    $arguments = explode('=', $server['argv'][0]);
    $offsetValue = $arguments[1] ?? '';

    //$server = ['offset' => $offsetValue];
    $updatesByOffset = [
        "ok" => true,
        "result" => [
            [
                "update_id" => $offsetValue,
                "message" => [
                    "message_id" => 197,
                    "from" => [
                        "id" => 655338422,
                        "is_bot" => false,
                        "first_name" => "Nickolay",
                        "language_code" => "ru"
                    ],
                    "chat" => [
                        "id" => 655338422,
                        "first_name" => "Nickolay",
                        "type" => "private"
                    ],
                    "date" => 1706326456,
                    "text" => "Test-Debug-Message-Nickolay"
                ]
            ]
        ]
    ];

    return $updatesByOffset;
}

header('Content-Type: application/json; charset=utf-8');

echo \json_encode(
    matchData($_SERVER['argc'], $_SERVER)
);

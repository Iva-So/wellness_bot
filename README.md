# Wellness_bot

## Getting started
```
git clone git@gitlab.com:Iva-So/wellness_bot.git
```
```
docker compose up --build -p
```
```
docker compose exec php-cli composer install
```
```
docker compose exec php-cli bin/console doctrine:migrations:migrate
```
```
docker compose exec php-cli bin/console doctrine:fixtures:load
```

## Telegram

1. We create our own bot in telegram use @botFather. See https://core.telegram.org/bots#how-do-i-create-a-bot
2. Get token your bot. See https://core.telegram.org/bots/tutorial#obtain-your-bot-token
3. Set menu command in your bot. See https://core.telegram.org/bots/features#edit-bots .See /setcommands.
   Set list commands in your bot:
```
   start - Старт
   help - Возможности бота
   training - Тренировки
   nutrition - Питание
   survey - Анкетирование
   daily - Мониторинг питания
   weight - Мониторинг веса
   recommendation - Получить рекомендации
```
4. Copy variables from .env.example in .env 
5. Set your bot token in .env to BOT_TELEGRAM_TOKEN variable


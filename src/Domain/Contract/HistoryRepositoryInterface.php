<?php

namespace App\Domain\Contract;

interface HistoryRepositoryInterface
{
    public function set(string $key, $value): void;

    public function get(string $key): ?string;

    public function add(string $chatId, string $questionId, string $answerText): void;

    public function count(string $chatId): int;

    public function extractLast(string $chatId): string;

    public function extractFirst(string $chatId): string;

    public function extractAll(string $chatId): array;
}

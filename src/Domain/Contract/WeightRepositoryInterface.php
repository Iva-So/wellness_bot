<?php

declare(strict_types=1);

namespace App\Domain\Contract;

interface WeightRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);

    public function findAll();

    public function findOneBy(array $criteria, array $orderBy = null);

    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null);

    public function persist(EntityInterface $entity): void;

    public function flush(): void;
}

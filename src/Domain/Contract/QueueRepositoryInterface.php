<?php

declare(strict_types=1);

namespace App\Domain\Contract;

interface QueueRepositoryInterface
{
    public function publish(string $key, $value): void;

    public function subscribe(string $key, \Closure $callback): void;
}

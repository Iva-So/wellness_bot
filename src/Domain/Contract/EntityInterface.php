<?php

namespace App\Domain\Contract;

interface EntityInterface
{
    public function getId(): ?int;
}

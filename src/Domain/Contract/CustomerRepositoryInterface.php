<?php

namespace App\Domain\Contract;

interface CustomerRepositoryInterface
{
    public function find($id, $lockMode = null, $lockVersion = null);

    public function findAll();

    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null);

    public function findOneBy(array $criteria, array $orderBy = null);

    public function persist(EntityInterface $entity): void;

    public function flush(): void;
}

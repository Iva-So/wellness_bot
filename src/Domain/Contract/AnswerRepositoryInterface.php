<?php

namespace App\Domain\Contract;

interface AnswerRepositoryInterface
{
    public function persist(EntityInterface $entity): void;

    public function flush(): void;
}

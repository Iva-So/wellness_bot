<?php

namespace App\Domain\Contract;

use App\Application\Exceptions\ElasticSearchDocumentSaveException;

interface ElasticSearchRepositoryInterface
{
    /**
     * @throws ElasticSearchDocumentSaveException
     */
    public function save(array $data): void;
}

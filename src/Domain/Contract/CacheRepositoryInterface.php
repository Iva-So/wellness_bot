<?php

namespace App\Domain\Contract;

interface CacheRepositoryInterface
{
    public function set(string $key, $value): void;

    public function get(string $key): ?string;
}

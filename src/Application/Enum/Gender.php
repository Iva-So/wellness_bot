<?php

declare(strict_types=1);

namespace App\Application\Enum;

enum Gender: string
{
    case MALE = 'Мужчина';
    case FEMALE = 'Женщина';
}

<?php

declare(strict_types=1);

namespace App\Application\Enum;

enum QuestionSequence: int
{
    case GENDER = 1;
    case AGE = 2;
    case JOB = 3;
    case HEIGHT = 4;
    case WEIGHT = 5;
}

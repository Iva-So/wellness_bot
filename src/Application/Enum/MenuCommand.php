<?php

declare(strict_types=1);

namespace App\Application\Enum;

enum MenuCommand: string
{
    case START = '/start';
    case HELP = '/help';
    case TRAINING = '/training';
    case NUTRITION = '/nutrition';
    case SURVEY = '/survey';
    case DAILY = '/daily';
    case RECOMMENDATION = '/recommendation';
    case WEIGHT = '/weight';
}

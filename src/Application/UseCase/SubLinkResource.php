<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\BotConfigInterface;
use App\Application\Enum\MenuCommand;
use App\Domain\Entity\Command;
use App\Infrastructure\Repository\CommandRepository;
use Doctrine\Common\Collections\Collection;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

class SubLinkResource
{
    private const BUTTONS_PER_ROW = 1;
    private BotApi $bot;

    public function __construct(
        private readonly BotConfigInterface $botConfig,
        private readonly CommandRepository  $commandRepository,
    ) {
        $this->bot = new BotApi($this->botConfig->getBotToken());
    }

    public function run($chatId, string $text, MenuCommand $commandName): void
    {
        $command = $this->getCommand($commandName->value);
        $resources = $command->getResources();

        $buttons = $this->getButtons($resources);
        $keyboard = new InlineKeyboardMarkup($buttons);

        $this->bot->sendMessage(
            $chatId,
            $text,
            null,
            false,
            null,
            $keyboard
        );
    }

    private function getCommand(string $commandName): Command
    {
        return $this->commandRepository->findOneBy(['name' => $commandName]);
    }

    private function getButtons(Collection $resources): array
    {
        $buttons = [];
        foreach ($resources as $i => $resource) {
            $button = [
                'text' => $resource->getDescription(),
                'url' => $resource->getLink(),
            ];
            $rowIndex = (int)($i / self::BUTTONS_PER_ROW);
            $buttons[$rowIndex][] = $button;
        }

        return $buttons;
    }
}

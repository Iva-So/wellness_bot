<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateNutrients;

class CalculateFat implements CalculateNutrientsInterface
{
    public function calc(int $dailyCalorie): float
    {
        $fat = $dailyCalorie * 0.3 / 9;

        return round($fat, 1);
    }
}

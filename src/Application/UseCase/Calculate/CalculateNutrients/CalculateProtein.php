<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateNutrients;

class CalculateProtein implements CalculateNutrientsInterface
{
    public function calc(int $dailyCalorie): float
    {
        $protein = $dailyCalorie * 0.3 / 4;

        return round($protein, 1);
    }
}

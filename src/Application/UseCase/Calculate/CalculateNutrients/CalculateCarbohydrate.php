<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateNutrients;

class CalculateCarbohydrate implements CalculateNutrientsInterface
{
    public function calc(int $dailyCalorie): float
    {
        $carbohydrate = $dailyCalorie * 0.4 / 4;

        return round($carbohydrate, 1);
    }
}

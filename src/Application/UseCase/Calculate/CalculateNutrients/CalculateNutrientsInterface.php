<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateNutrients;

interface CalculateNutrientsInterface
{
    public function calc(int $dailyCalorie): int|float;
}

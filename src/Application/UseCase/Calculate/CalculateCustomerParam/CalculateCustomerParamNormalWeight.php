<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateCustomerParam;

use App\Application\DTO\CustomerParamDto;
use App\Application\Enum\Gender;

class CalculateCustomerParamNormalWeight implements CalculateCustomerParamInterface
{
    public function calc(CustomerParamDto $dto): float
    {
        if (Gender::FEMALE->value === $dto->gender->getGender()) {
            return $dto->height->getHeight() - (100 + ($dto->height->getHeight() - 100) / 10);
        }

        return $dto->height->getHeight() - (100 + ($dto->height->getHeight() - 100) / 20);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateCustomerParam;

use App\Application\DTO\CustomerParamDto;

interface CalculateCustomerParamInterface
{
    public function calc(CustomerParamDto $dto): int|float;
}

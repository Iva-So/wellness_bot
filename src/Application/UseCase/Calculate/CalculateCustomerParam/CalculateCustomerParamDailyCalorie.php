<?php

declare(strict_types=1);

namespace App\Application\UseCase\Calculate\CalculateCustomerParam;

use App\Application\DTO\CustomerParamDto;
use App\Application\Enum\Gender;

class CalculateCustomerParamDailyCalorie implements CalculateCustomerParamInterface
{
    public function calc(CustomerParamDto $dto): int
    {
        if (Gender::FEMALE->value === $dto->gender->getGender()) {
            return (int) ((10 * $dto->weight->getWeight()) + (6.25 * $dto->height->getHeight()) - (5 * $dto->age->getAge()) - 161);
        }

        return (int) ((10 * $dto->weight->getWeight()) + (6.25 * $dto->height->getHeight()) - (5 * $dto->age->getAge()) + 5);
    }
}

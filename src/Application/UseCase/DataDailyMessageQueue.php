<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Service\ElasticSaveProductService;
use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Contract\ProductRepositoryInterface;
use App\Domain\Contract\QueueRepositoryInterface;
use App\Domain\Entity\Product;
use App\Infrastructure\Log\LogInterface;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

readonly class DataDailyMessageQueue
{
    public function __construct(
        private ElasticSaveProductService $elasticSaveProductService,
        private QueueRepositoryInterface $queueRepository,
        private ProductRepositoryInterface $productRepository,
        private CustomerRepositoryInterface $customerRepository,
        private LogInterface $log
    ) {
    }

    public function run(): void
    {
        $this->queueRepository->subscribe('daily', function ($message) {
            $this->sendToRepository($message->body);
        });
    }

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function sendToRepository(string $message): void
    {
        try {
            [$chatId, $answerText] = $this->getChatIdWithMessageText($message);
            $this->log->write("** chaId `$chatId` message `$answerText`", LogInterface::LOG_INFO);
            $productFromElastic = $this->elasticSaveProductService->getProduct($answerText);
            $this->log->write("** Product name: {$productFromElastic->getName()}", LogInterface::LOG_INFO);
            $this->log->write("** Searching customer with chaId `$chatId` ", LogInterface::LOG_INFO);
            $customer = $this->customerRepository->findOneBy(['chatId' => $chatId]);
            $this->log->write('** Customer-id: ' . $customer->getId(), LogInterface::LOG_INFO);
            if (!empty($customer)) {
                $product = new Product();
                $product->setCustomer($customer);
                $product->setCreatedAtAutomatically();
                $product->setName($productFromElastic->getName());
                $product->setCalorie($productFromElastic->getCalories());
                $product->setWeight($productFromElastic->getServingSizeGram());
                $product->setCarbohydrate($productFromElastic->getCarbohydratesTotalGram());
                $product->setFat($productFromElastic->getFatTotalGram());
                $product->setProtein($productFromElastic->getProteinGram());
                $this->productRepository->persist($product);
                $this->productRepository->flush();
                $this->log->write('Product ' . $productFromElastic->getName() . ' save to DB successfully', LogInterface::LOG_INFO);
            }
        } catch (Exception $exception) {
            $this->log->write('Save product to db failed. Message: ' . $exception->getMessage(), LogInterface::LOG_ERROR);
            throw new Exception('Save product to db failed. Message: ' . $exception->getMessage());
        }
    }

    private function getChatIdWithMessageText(string $textBinary): array
    {
        preg_match('/chat:(\d+);message:(.*)/', $textBinary, $matches);

        return [$matches[1], $matches[2]];
    }
}

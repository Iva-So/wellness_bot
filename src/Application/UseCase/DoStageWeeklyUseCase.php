<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Domain\Contract\QueueRepositoryInterface;
use App\Infrastructure\Log\LogInterface;
use Exception;

class DoStageWeeklyUseCase implements UseCaseInterface
{
    public function __construct(
        private readonly LogInterface $log,
        private readonly QueueRepositoryInterface $queueRepository
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        try {
            $chatId = $lastShortUpdate->getUserId();
            $messageOriginalText = $lastShortUpdate->getMessageText();
            $messageToQueue = "chat:$chatId;message:$messageOriginalText";

            $messageLog = "Send message `$messageToQueue` to queue `weekly`";
            $this->log->write($messageLog, LogInterface::LOG_DEBUG);
            $this->queueRepository->publish('weight', $messageToQueue);
        } catch (Exception $exception) {
            $this->log->write($exception->getMessage(), LogInterface::LOG_ERROR);
        }
    }

}

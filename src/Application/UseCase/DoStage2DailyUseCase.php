<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Domain\Contract\QueueRepositoryInterface;
use App\Infrastructure\Log\LogInterface;
use App\Application\Service\Validator\ValidatorInterface;
use Exception;

readonly class DoStage2DailyUseCase implements UseCaseInterface
{
    public function __construct(
        private LogInterface $log,
        private QueueRepositoryInterface $queueRepository,
        private readonly ValidatorInterface $validator
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        try {
            $chatId = $lastShortUpdate->getChatId();
            $messageOriginalText = $lastShortUpdate->getMessageText();
            $messageToQueue = "chat:$chatId;message:$messageOriginalText";
            $isValidMessage = $this->validator->isValid($chatId, $messageOriginalText);

            if ($isValidMessage) {
                $messageLog = "Send message `$messageToQueue` to queue `daily`";
                $this->log->write($messageLog, LogInterface::LOG_DEBUG);
                $this->queueRepository->publish('daily', $messageToQueue);
            }
        } catch (Exception $exception) {
            $this->log->write($exception->getMessage(), LogInterface::LOG_ERROR);
        }
    }
}

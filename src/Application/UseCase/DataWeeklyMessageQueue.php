<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Contract\QueueRepositoryInterface;
use App\Domain\Contract\WeightRepositoryInterface;
use App\Domain\Entity\Weight;
use App\Infrastructure\Log\LogInterface;
use Exception;

class DataWeeklyMessageQueue
{
    public function __construct(
        private readonly QueueRepositoryInterface    $queueRepository,
        private readonly CustomerRepositoryInterface $customerRepository,
        private readonly WeightRepositoryInterface   $weightRepository,
        private readonly LogInterface $log
    ) {
    }

    public function run(): void
    {
        $this->queueRepository->subscribe('weight', function ($message) {
            $this->sendToRepository($message->body);
        });
    }
    public function sendToRepository(string $message): void
    {
        try {
            $msg = "@ToDo: Send answer `$message` to repository";
            $this->log->write($msg, LogInterface::LOG_INFO);
            [$chatId, $answerText] = $this->getChatIdWithMessageText($message);
            $customer = $this->customerRepository->findOneBy(['chatId' => $chatId]);
            if (!empty($customer)) {
                $weight = new Weight();
                $weight->setCustomer($customer);
                $weight->setCreatedAtAutomatically();
                $weight->setCurrentWeight((float) $answerText);

                $this->weightRepository->persist($weight);
                $this->weightRepository->flush();
                $this->log->write("@ToDo: Use values chatId:$chatId, answerText:$answerText", LogInterface::LOG_INFO);
            }
        } catch (Exception $exception) {
            throw new Exception('Save weight to db failed. Message: ' . $exception->getMessage());
        }
    }

    private function getChatIdWithMessageText(string $textBinary): array
    {
        preg_match('/chat:(\d+);message:(.*)/', $textBinary, $matches);

        return [$matches[1], $matches[2]];
    }
}

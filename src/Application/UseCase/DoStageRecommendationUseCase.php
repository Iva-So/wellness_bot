<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Application\Exceptions\RecommendationException;
use App\Application\UseCase\MenuCommand\AnswerRecommendationCommand;
use App\Infrastructure\Log\LogInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

readonly class DoStageRecommendationUseCase implements UseCaseInterface
{
    public function __construct(
        private LogInterface $log,
        private AnswerRecommendationCommand $answerRecommendationCommand
    ) {
    }

    /**
     * @throws InvalidArgumentException
     * @throws RecommendationException
     * @throws Exception
     * @throws NonUniqueResultException
     * @throws \Doctrine\DBAL\Exception
     * @throws NoResultException
     */
    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $this->answerRecommendationCommand->run();
        $this->log->write('done', LogInterface::LOG_INFO);
    }
}

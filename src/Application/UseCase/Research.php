<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTO;

class Research
{
    public function __construct(
        private readonly GetLastMessageDTOFromBotAPI $getLastMessageDTOFromBotAPI,
        private readonly SubSurveyUseCase $subSurveyUseCase
    ) {
    }

    public function run(): void
    {
        $lastUpdate = $this->getLastMessageDTOFromBotAPI->run();

        $shortMessageDTO = new ShortMessageDTO(
            (string) $lastUpdate->message->chat->id,
            $lastUpdate->message->text,
            $lastUpdate->message->chat->firstName
        );

        $this->subSurveyUseCase->run($shortMessageDTO);
    }
}

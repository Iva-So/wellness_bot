<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\BotConfigInterface;
use App\Application\DTO\ShortMessageDTO;
use App\Application\DTO\ShortMessageDTOInterface;
use App\Domain\Contract\CacheRepositoryInterface;
use App\Infrastructure\Log\LogInterface as Log;
use Exception;

class ChatWithLastMessageBotAPI
{
    public function __construct(
        private readonly CacheRepositoryInterface $cacheRepository,
        private readonly BotConfigInterface $botConfig,
        private readonly Log $log,
        private readonly SendResponseMessageToBotAPI $sendResponse
    ) {
    }

    /**
     * @throws Exception
     */
    public function run(): bool
    {
        $apiUrl = $this->botConfig->getApiUrl();
        $uniqHash = hash('sha256', $apiUrl);
        $lastUpdateIdKey = substr($uniqHash, 0, 16);
        $lastUpdateIdKey = "app-hash:$lastUpdateIdKey:lastUpdateId";
        $previousUpdateId = $this->cacheRepository->get($lastUpdateIdKey) ?? false;
        $this->log->write('previousUpdateId: ' . $previousUpdateId, Log::LOG_DEBUG);

        $offsetSubQuery = '?offset=' . ($previousUpdateId + 1);
        $argSubQuery = $previousUpdateId ? $offsetSubQuery : '';
        $url = $apiUrl . $argSubQuery;

        $this->log->write("Sending request `$url` to bot-api.", Log::LOG_DEBUG);
        $update = file_get_contents($url);
        $updateArray = json_decode($update, true);

        $resultCount = count($updateArray['result']);
        $this->log->write('updates.result count: ' . $resultCount, Log::LOG_DEBUG);
        $this->log->write(var_export($updateArray, true), Log::LOG_DEBUG);
        if (!$resultCount) {
            $this->log->write('updates are empty.', Log::LOG_INFO);

            return false;
        }

        $lastUpdateId = null;
        $chatIds = [];
        foreach ($updateArray['result'] ?? [] as $currentElement) {
            $message = $currentElement['message'];
            $chatId = $message['chat']['id'];
            $chatIds[$chatId] = new ShortMessageDTO($chatId, (string) $message['text']);
            $lastUpdateId = $currentElement['update_id'];
        }
        $this->log->write('chatIds: ' . var_export($chatIds, true), Log::LOG_DEBUG);
        $this->log->write('lastUpdateId: ' . $lastUpdateId, Log::LOG_DEBUG);

        /** @var ShortMessageDTOInterface $message */
        foreach ($chatIds as $chatId => $message) {
            $this->log->write("Sending message `{$message->getMessageText()}` to chat `$chatId` ...", Log::LOG_DEBUG);
            // $this->sendResponse->run("Your message `{$message->getMessageText()}` will calculate soon", $chatId);
            // check sent the message
        }
        $this->cacheRepository->set($lastUpdateIdKey, $lastUpdateId);

        $this->log->write('done.', Log::LOG_INFO);

        return true;
    }
}

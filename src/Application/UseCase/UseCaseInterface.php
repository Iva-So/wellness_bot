<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;

interface UseCaseInterface
{
    public function run(ShortMessageDTOInterface $lastShortUpdate): void;
}

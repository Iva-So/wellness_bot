<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Application\Enum\MenuCommand;
use App\Infrastructure\Log\LogInterface;

readonly class DoStageNutritionUseCase implements UseCaseInterface
{
    public function __construct(
        private LogInterface $log,
        private SubLinkResource $subLinkResource
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $this->subLinkResource->run(
            $lastShortUpdate->getUserId(),
            'Питание youtube-формат',
            MenuCommand::NUTRITION
        );

        $this->log->write('done', LogInterface::LOG_INFO);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;

readonly class DoStage1SurveyUseCase implements UseCaseInterface
{
    public function __construct(
        private SubSurveyUseCase $subSurveyUseCase
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $this->subSurveyUseCase->run($lastShortUpdate);
    }
}

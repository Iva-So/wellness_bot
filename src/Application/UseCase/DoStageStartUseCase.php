<?php

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Application\Service\CustomerService;

class DoStageStartUseCase implements UseCaseInterface
{
    public function __construct(private readonly CustomerService $customerService)
    {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $this->customerService->save($lastShortUpdate->getFirstName(), $lastShortUpdate->getUserId());
    }
}

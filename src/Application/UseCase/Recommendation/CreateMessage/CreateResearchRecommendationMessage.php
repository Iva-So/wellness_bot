<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\CreateMessage;

use App\Application\DTO\RecommendationDTO;

class CreateResearchRecommendationMessage implements CreateRecommendationMessageInterface
{
    private const RECOMMENDATION_TEMPLATE = 'По рекомендациям ВОЗ ваш вес должен составлять: %s кг, дневной калораж: %s ккал. Из этого калоража белки составят: %s г., жиры: %s г., углеводы: %s г.';

    public function create(RecommendationDTO $recommendationDto): string
    {
        return sprintf(
            self::RECOMMENDATION_TEMPLATE,
            $recommendationDto->customerWeight,
            $recommendationDto->dailyCalorie,
            $recommendationDto->protein,
            $recommendationDto->fat,
            $recommendationDto->carbohydrate
        );
    }
}

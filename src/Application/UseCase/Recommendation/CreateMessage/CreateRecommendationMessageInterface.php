<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\CreateMessage;

use App\Application\DTO\RecommendationDTO;

interface CreateRecommendationMessageInterface
{
    public function create(RecommendationDTO $recommendationDto): string;
}

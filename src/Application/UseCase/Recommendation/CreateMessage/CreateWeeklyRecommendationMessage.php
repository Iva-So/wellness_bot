<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\CreateMessage;

use App\Application\DTO\RecommendationDTO;
use App\Infrastructure\Log\LogInterface;

class CreateWeeklyRecommendationMessage implements CreateRecommendationMessageInterface
{
    private const WEEKLY_RECOMMENDATION_GAIN_WEIGHT_TEMPLATE = 'На данном этапе ваш вес, отправленный вами %s превышает рекомендованный на %s кг. Постарайтесь придерживаться рекомендованной дневной калорийности %s ккал.';
    private const WEEKLY_RECOMMENDATION_LOSE_WEIGHT_TEMPLATE = 'На данном этапе ваш вес, отправленный вами %s меньше рекомендованного на %s кг. Постарайтесь придерживаться рекомендованной дневной калорийности %s ккал.';
    private const WEEKLY_RECOMMENDATION_ERROR_SURVEY_TEMPLATE = 'Для рекомендации по мониторингу веса необходимо пройти анкетирование. Пройдите анкетирование, нажав в меню пункт "Анкетирование".';
    private const WEEKLY_RECOMMENDATION_ERROR_WEIGHT_TEMPLATE = 'У вас отсутствует результат мониторинга веса. Пройдите мониторинг веса, нажав в меню пункт "Мониторинг веса". Либо дождитесь меня, каждое утро я запускаю опрос.';
    private const WEEKLY_RECOMMENDATION_SUCCESS_TEMPLATE = 'Ваш вес составляет %s кг - это нормальный для вас вес по рекомендациям ВОЗ. Поздравляю!';

    public function __construct(private readonly LogInterface $log)
    {
    }

    public function create(RecommendationDTO $recommendationDto): string
    {
        if ($recommendationDto->lastRecommendation === null) {
            $this->log->write("Customer {$recommendationDto->customer->getId()} has no survey recommendation. Take a survey.", LogInterface::LOG_INFO);
            return self::WEEKLY_RECOMMENDATION_ERROR_SURVEY_TEMPLATE;
        }

        if ($recommendationDto->customerWeight === null) {
            $this->log->write("Customer {$recommendationDto->customer->getId()} has no weekly weight. Take a weight survey.", LogInterface::LOG_INFO);
            return self::WEEKLY_RECOMMENDATION_ERROR_WEIGHT_TEMPLATE;
        }

        return $this->createRecommendation($recommendationDto);
    }

    private function createRecommendation(RecommendationDTO $recommendationDto): string
    {
        $currentWeight = $recommendationDto->customerWeight;
        $recommendationWeight = $recommendationDto->lastRecommendation->getWeight();
        $recommendationCalorie = $recommendationDto->lastRecommendation->getCalorie();
        $weightDate = $recommendationDto->createdAtWeeklyWeight;

        if ($currentWeight === $recommendationWeight) {
            $recommendation = sprintf(
                self::WEEKLY_RECOMMENDATION_SUCCESS_TEMPLATE,
                $currentWeight,
            ) . PHP_EOL;
        } elseif ($currentWeight > $recommendationWeight) {
            $recommendation = sprintf(
                self::WEEKLY_RECOMMENDATION_GAIN_WEIGHT_TEMPLATE,
                $weightDate,
                $currentWeight - $recommendationWeight,
                $recommendationCalorie
            ) . PHP_EOL;
        } else {
            $recommendation = sprintf(
                self::WEEKLY_RECOMMENDATION_LOSE_WEIGHT_TEMPLATE,
                $weightDate,
                $recommendationWeight - $currentWeight,
                $recommendationCalorie
            ) . PHP_EOL;
        }

        return $recommendation;
    }
}

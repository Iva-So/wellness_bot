<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\CreateMessage;

use App\Application\DTO\RecommendationDTO;
use App\Infrastructure\Log\LogInterface as Log;

class CreateDailyRecommendationMessage implements CreateRecommendationMessageInterface
{
    private const DAILY_RECOMMENDATION_TEMPLATE = 'За сегодня ваш дневной калораж составил: %s ккал. Из этого калоража белки составили: %s г., жиры: %s г., углеводы: %s г.';
    private const DAILY_RECOMMENDATION_DEFICIT_TEMPLATE = 'У вас есть дефицит %s на %s г. Постарайтесь повысить потребление %s';
    private const DAILY_RECOMMENDATION_PROFICIT_TEMPLATE = 'У вас есть профицит %s на %s г. Постарайтесь снизить потребление %s';
    private const DAILY_RECOMMENDATION_ERROR_SURVEY_TEMPLATE = 'Для рекомендации по мониторингу питания необходимо пройти анкетирование. Пройдите анкетирование, нажав в меню пункт "Анкетирование".';
    private const DAILY_RECOMMENDATION_ERROR_WEIGHT_TEMPLATE = 'Сегодня у вас отсутствует результат мониторинга питания. Пройдите мониторинг, нажав в меню пункт "Мониторинг питания" и добавьте продукты, которые сегодня съели.';
    private const DAILY_RECOMMENDATION_SUCCESS_TEMPLATE = 'У вас в норме количество %s - %s г. Поздравляю!';

    public function __construct(private readonly Log $log)
    {
    }

    public function create(RecommendationDTO $recommendationDto): string
    {
        if ($recommendationDto->lastRecommendation === null) {
            $this->log->write("Customer {$recommendationDto->customer->getId()} has no survey recommendation. Take a survey.", Log::LOG_INFO);
            return self::DAILY_RECOMMENDATION_ERROR_SURVEY_TEMPLATE;
        }

        if ($recommendationDto->dailyCalorie === null) {
            $this->log->write("Customer {$recommendationDto->customer->getId()} has no daily products. Take a daily survey.", Log::LOG_INFO);
            return self::DAILY_RECOMMENDATION_ERROR_WEIGHT_TEMPLATE;
        }

        $generalRecommendation = sprintf(
            self::DAILY_RECOMMENDATION_TEMPLATE,
            $recommendationDto->dailyCalorie,
            $recommendationDto->protein,
            $recommendationDto->fat,
            $recommendationDto->carbohydrate
        ) . PHP_EOL;

        $specificRecommendation = $this->createSpecificRecommendation($recommendationDto);

        return $generalRecommendation . $specificRecommendation;
    }

    private function createSpecificRecommendation(RecommendationDTO $recommendationDto): string
    {
        return $this->createFatRecommendation($recommendationDto)
            . $this->createProteinRecommendation($recommendationDto)
            . $this->createCarbohydrateRecommendation($recommendationDto);
    }

    private function createFatRecommendation(RecommendationDTO $recommendationDto): string
    {
        $currentFat = $recommendationDto->fat;
        $recommendationFat = $recommendationDto->lastRecommendation->getFat();

        if ($currentFat === $recommendationFat) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_SUCCESS_TEMPLATE,
                'жиров',
                $currentFat,
            ) . PHP_EOL;
        } elseif ($currentFat > $recommendationFat) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_PROFICIT_TEMPLATE,
                'жиров',
                $currentFat - $recommendationFat,
                'жиров',
            ) . PHP_EOL;
        } else {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_DEFICIT_TEMPLATE,
                'жиров',
                $recommendationFat - $currentFat,
                'жиров',
            ) . PHP_EOL;
        }

        return $specificRecommendation;
    }

    private function createProteinRecommendation(RecommendationDTO $recommendationDto): string
    {
        $currentProtein = $recommendationDto->protein;
        $recommendationProtein = $recommendationDto->lastRecommendation->getProtein();

        if ($currentProtein === $recommendationProtein) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_SUCCESS_TEMPLATE,
                'белка',
                $currentProtein,
            ) . PHP_EOL;
        } elseif ($currentProtein > $recommendationProtein) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_PROFICIT_TEMPLATE,
                'белка',
                $currentProtein - $recommendationProtein,
                'белка',
            ) . PHP_EOL;
        } else {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_DEFICIT_TEMPLATE,
                'белка',
                $recommendationProtein - $currentProtein,
                'белка',
            ) . PHP_EOL;
        }

        return $specificRecommendation;
    }

    private function createCarbohydrateRecommendation(RecommendationDTO $recommendationDto): string
    {
        $currentCarbohydrate = $recommendationDto->carbohydrate;
        $recommendationCarbohydrate = $recommendationDto->lastRecommendation->getCarbohydrate();

        if ($currentCarbohydrate === $recommendationCarbohydrate) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_SUCCESS_TEMPLATE,
                'углеводов',
                $currentCarbohydrate,
            ) . PHP_EOL;
        } elseif ($currentCarbohydrate > $recommendationCarbohydrate) {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_PROFICIT_TEMPLATE,
                'углеводов',
                $currentCarbohydrate - $recommendationCarbohydrate,
                'углеводов',
            ) . PHP_EOL;
        } else {
            $specificRecommendation = sprintf(
                self::DAILY_RECOMMENDATION_DEFICIT_TEMPLATE,
                'углеводов',
                $recommendationCarbohydrate - $currentCarbohydrate,
                'углеводов',
            ) . PHP_EOL;
        }

        return $specificRecommendation;
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\Send;

use App\Application\DTO\RecommendationDTO;
use App\Application\UseCase\Recommendation\CreateMessage\CreateDailyRecommendationMessage;
use App\Application\UseCase\SendResponseMessageToBotAPI;
use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Contract\ProductRepositoryInterface;
use App\Domain\Contract\RecommendationRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

class SendDailyRecommendation
{
    public function __construct(
        private readonly CustomerRepositoryInterface $customerRepository,
        private readonly ProductRepositoryInterface $productRepository,
        private readonly RecommendationRepositoryInterface $recommendationRepository,
        private readonly CreateDailyRecommendationMessage $message,
        private readonly SendResponseMessageToBotAPI $sendResponseMessage
    ) {
    }

    /**
     * @throws InvalidArgumentException
     * @throws Exception
     * @throws NonUniqueResultException
     * @throws \Doctrine\DBAL\Exception
     */
    public function run(): void
    {
        $customers = $this->customerRepository->findAll();
        foreach ($customers as $customer) {
            $lastRecommendation = $this->recommendationRepository->getLastResearchRecommendation($customer);
            $sumDailyCalorie = $this->productRepository->getSumDailyKBZHU($customer);

            $recommendationDto = new RecommendationDTO();
            $recommendationDto->customer = $customer;
            $recommendationDto->lastRecommendation = $lastRecommendation;
            $recommendationDto->dailyCalorie = (int)$sumDailyCalorie['daily_calorie'];
            $recommendationDto->protein = (float) $sumDailyCalorie['protein'];
            $recommendationDto->fat = (float) $sumDailyCalorie['fat'];
            $recommendationDto->carbohydrate = (float) $sumDailyCalorie['carbohydrate'];

            $msg = $this->message->create($recommendationDto);

            $this->sendResponseMessage->run($msg, $customer->getChatId());
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\Send;

use App\Application\DTO\CustomerParamDto;
use App\Application\DTO\RecommendationDTO;
use App\Application\DTO\ValueObject\Age;
use App\Application\DTO\ValueObject\Gender;
use App\Application\DTO\ValueObject\Height;
use App\Application\DTO\ValueObject\Weight;
use App\Application\Enum\QuestionSequence;
use App\Application\Exceptions\AgeException;
use App\Application\Exceptions\GenderException;
use App\Application\Exceptions\HeightException;
use App\Application\Exceptions\RecommendationException;
use App\Application\Exceptions\WeightException;
use App\Application\UseCase\Calculate\CalculateCustomerParam\CalculateCustomerParamDailyCalorie;
use App\Application\UseCase\Calculate\CalculateCustomerParam\CalculateCustomerParamNormalWeight;
use App\Application\UseCase\Calculate\CalculateNutrients\CalculateCarbohydrate;
use App\Application\UseCase\Calculate\CalculateNutrients\CalculateFat;
use App\Application\UseCase\Calculate\CalculateNutrients\CalculateProtein;
use App\Application\UseCase\Helper\ExplodeHelper;
use App\Application\UseCase\Recommendation\CreateMessage\CreateResearchRecommendationMessage;
use App\Application\UseCase\SendResponseMessageToBotAPI;
use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Contract\RecommendationRepositoryInterface;
use App\Domain\Entity\Recommendation;
use App\Infrastructure\Log\LogInterface;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

class SendResearchRecommendation
{
    public function __construct(
        private readonly CalculateCustomerParamDailyCalorie $calcDailyCalorie,
        private readonly CalculateCustomerParamNormalWeight $calcNormalWeight,
        private readonly SendResponseMessageToBotAPI $sendResponseMessage,
        private readonly CustomerRepositoryInterface $customerRepository,
        private readonly RecommendationRepositoryInterface $recommendationRepository,
        private readonly CreateResearchRecommendationMessage $createRecommendationMessage,
        private readonly CalculateFat $fat,
        private readonly CalculateProtein $protein,
        private readonly CalculateCarbohydrate $carbohydrate,
        private readonly LogInterface $log
    ) {
    }

    /**
     * @throws AgeException
     * @throws InvalidArgumentException
     * @throws RecommendationException
     * @throws HeightException
     * @throws Exception
     * @throws WeightException
     * @throws GenderException
     */
    public function run(array $dataAfterResearch, string $chatId): void
    {
        $customer = $this->customerRepository->findOneBy(['chatId' => $chatId]);
        $customerParamDto = $this->setParamCustomerDto($dataAfterResearch);

        $dailyCalorie = $this->calcDailyCalorie->calc($customerParamDto);
        $normalWeight = $this->calcNormalWeight->calc($customerParamDto);
        $protein = $this->protein->calc($dailyCalorie);
        $fat = $this->fat->calc($dailyCalorie);
        $carbohydrate = $this->carbohydrate->calc($dailyCalorie);

        $recommendationDto = new RecommendationDTO();
        $recommendationDto->customer = $customer;
        $recommendationDto->dailyCalorie = $dailyCalorie;
        $recommendationDto->customerWeight = $normalWeight;
        $recommendationDto->protein = $protein;
        $recommendationDto->fat = $fat;
        $recommendationDto->carbohydrate = $carbohydrate;

        $msgRecommendation = $this->createRecommendationMessage->create($recommendationDto);

        $this->sendResponseMessage->run($msgRecommendation, $chatId);
        $this->newRecommendation($recommendationDto);
    }

    /**
     * @throws WeightException
     * @throws AgeException
     * @throws HeightException
     * @throws GenderException
     * @throws RecommendationException
     */
    private function setParamCustomerDto(array $dataAfterResearch): CustomerParamDto
    {
        $customerParamDto = new CustomerParamDto();

        foreach ($dataAfterResearch as $item) {
            $sequenceQuestion = ExplodeHelper::getSequenceQuestion($item);
            $answer = ExplodeHelper::getAnswer($item);

            match ($sequenceQuestion) {
                QuestionSequence::AGE->value => $customerParamDto->age = new Age($answer),
                QuestionSequence::WEIGHT->value => $customerParamDto->weight = new Weight($answer),
                QuestionSequence::HEIGHT->value => $customerParamDto->height = new Height($answer),
                QuestionSequence::GENDER->value => $customerParamDto->gender = new Gender($answer),
                default => null
            };
        }

        if (
            null === $customerParamDto->gender
            || null === $customerParamDto->height
            || null === $customerParamDto->weight
            || null === $customerParamDto->age
        ) {
            $this->log->write('Please answer all questions', LogInterface::LOG_ERROR);
            throw new RecommendationException('Please answer all questions');
        }

        return $customerParamDto;
    }

    private function newRecommendation(RecommendationDTO $recommendationDto): void
    {
        $recommendation = new Recommendation();
        $recommendation->setCustomer($recommendationDto->customer);
        $recommendation->setCalorie($recommendationDto->dailyCalorie);
        $recommendation->setWeight($recommendationDto->customerWeight);
        $recommendation->setFat($recommendationDto->fat);
        $recommendation->setProtein($recommendationDto->protein);
        $recommendation->setCarbohydrate($recommendationDto->carbohydrate);
        $recommendation->setCreatedAtAutomatically();
        $recommendation->setUpdatedAtAutomatically();

        $this->recommendationRepository->persist($recommendation);
        $this->recommendationRepository->flush();
    }
}

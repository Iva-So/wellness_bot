<?php

declare(strict_types=1);

namespace App\Application\UseCase\Recommendation\Send;

use App\Application\DTO\RecommendationDTO;
use App\Application\Exceptions\RecommendationException;
use App\Application\UseCase\Recommendation\CreateMessage\CreateWeeklyRecommendationMessage;
use App\Application\UseCase\SendResponseMessageToBotAPI;
use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Contract\RecommendationRepositoryInterface;
use App\Domain\Contract\WeightRepositoryInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

class SendWeeklyRecommendation
{
    public function __construct(
        private readonly WeightRepositoryInterface $weightRepository,
        private readonly RecommendationRepositoryInterface $recommendationRepository,
        private readonly CustomerRepositoryInterface $customerRepository,
        private readonly SendResponseMessageToBotAPI $sendResponseMessage,
        private readonly CreateWeeklyRecommendationMessage $createRecommendationMessage
    ) {
    }

    /**
     * @throws InvalidArgumentException
     * @throws RecommendationException
     * @throws NonUniqueResultException
     * @throws Exception
     * @throws NoResultException
     */
    public function run(): void
    {
        $customers = $this->customerRepository->findAll();

        foreach ($customers as $customer) {
            $lastRecommendation = $this->recommendationRepository->getLastResearchRecommendation($customer);
            $lastWeeklyWeight = $this->weightRepository->getLastWeightByCustomer($customer);

            $recommendationDto = new RecommendationDTO();
            $recommendationDto->customer = $customer;
            $recommendationDto->lastRecommendation = $lastRecommendation;
            $recommendationDto->customerWeight = $lastWeeklyWeight?->getCurrentWeight();
            $recommendationDto->createdAtWeeklyWeight = $lastWeeklyWeight?->getCreatedAt()->format('d-m-Y');

            $msg = $this->createRecommendationMessage->create($recommendationDto);

            $this->sendResponseMessage->run($msg, $customer->getChatId());
        }
    }
}

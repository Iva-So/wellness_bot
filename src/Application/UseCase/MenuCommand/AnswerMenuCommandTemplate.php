<?php

declare(strict_types=1);

namespace App\Application\UseCase\MenuCommand;

use App\Application\Constants\BotConfigInterface;
use App\Application\UseCase\GetLastMessageDTOFromBotAPI;
use App\Domain\Contract\CommandRepositoryInterface;
use App\Domain\Entity\Command;
use TelegramBot\Api\BotApi;

abstract class AnswerMenuCommandTemplate
{
    protected BotApi $bot;

    /**
     * @throws \Exception
     */
    public function __construct(
        protected readonly GetLastMessageDTOFromBotAPI $fromBotAPI,
        protected readonly CommandRepositoryInterface $commandRepository,
        protected readonly BotConfigInterface $botConfig,
    ) {
        $this->bot = new BotApi($this->botConfig->getBotToken());
    }

    public function run(): void
    {
        $command = $this->getCommand();
        $getUpdate = $this->fromBotAPI->run();
        $this->bot->sendMessage($getUpdate->message->chat->id, $command->getText());
    }

    abstract protected function getCommand(): Command;
}

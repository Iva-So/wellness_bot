<?php

declare(strict_types=1);

namespace App\Application\UseCase\MenuCommand;

use App\Application\Enum\MenuCommand;
use App\Domain\Entity\Command;
use Doctrine\Common\Collections\Collection;
use TelegramBot\Api\Types\Inline\InlineKeyboardMarkup;

class AnswerTrainingCommand extends AnswerMenuCommandTemplate
{
    private const BUTTONS_PER_ROW = 1;

    public function run(): void
    {
        $command = $this->getCommand();
        $resources = $command->getResources();
        $getUpdate = $this->fromBotAPI->run();

        $buttons = $this->getButtons($resources);
        $keyboard = new InlineKeyboardMarkup($buttons);

        $this->bot->sendMessage(
            $getUpdate->message->chat->id,
            $command->getText(),
            null,
            false,
            null,
            $keyboard
        );
    }

    protected function getCommand(): Command
    {
        return $this->commandRepository->findOneBy([
            'name' => MenuCommand::TRAINING,
        ]);
    }

    protected function getButtons(Collection $resources): array
    {
        $buttons = [];
        $row = [];
        foreach ($resources as $resource) {
            $button = [
                'text' => $resource->getDescription(),
                'url' => $resource->getLink(),
            ];

            $row[] = $button;
            if (self::BUTTONS_PER_ROW === count($row)) {
                $buttons[] = $row;
                $row = [];
            }
        }

        if (!empty($row)) {
            $buttons[] = $row;
        }

        return $buttons;
    }
}

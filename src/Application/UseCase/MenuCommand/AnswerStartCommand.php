<?php

declare(strict_types=1);

namespace App\Application\UseCase\MenuCommand;

use App\Application\Enum\MenuCommand;
use App\Domain\Entity\Command;

class AnswerStartCommand extends AnswerMenuCommandTemplate
{
    protected function getCommand(): Command
    {
        return $this->commandRepository->findOneBy([
            'name' => MenuCommand::START,
        ]);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase\MenuCommand;

use App\Application\Exceptions\RecommendationException;
use App\Application\UseCase\Recommendation\Send\SendDailyRecommendation;
use App\Application\UseCase\Recommendation\Send\SendWeeklyRecommendation;
use App\Infrastructure\Log\LogInterface;
use Throwable;

class AnswerRecommendationCommand
{
    public function __construct(
        private readonly SendWeeklyRecommendation $sendWeeklyRecommendation,
        private readonly SendDailyRecommendation $sendDailyRecommendation,
        private readonly LogInterface $log
    ) {
    }

    public function run(): void
    {
        try {
            $this->sendDailyRecommendation->run();
            $this->sendWeeklyRecommendation->run();
        } catch (Throwable $e) {
            $this->log->write(PHP_EOL . $e->getFile() .PHP_EOL . $e->getLine() . PHP_EOL . $e->getMessage(), LogInterface::LOG_ERROR);
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\BotConfigInterface;
use App\Application\DTO\UpdateDTO;
use App\Infrastructure\Log\LogInterface as Log;
use Exception;

class GetLastMessageDTOFromBotAPI
{
    public function __construct(
        private readonly BotConfigInterface $botConfig,
        private readonly Log $log
    ) {
    }

    /**
     * @throws Exception
     */
    public function run(): UpdateDTO
    {
        $url = $this->botConfig->getApiUrl();

        $this->log->write("Sending request `$url` to bot-api.", Log::LOG_DEBUG);
        $update = file_get_contents($url);
        $updateArray = json_decode($update, true);

        $this->log->write('updates.result ...', Log::LOG_DEBUG);
        $this->log->write(var_export($updateArray, true), Log::LOG_DEBUG);

        $lastUpdate = end($updateArray['result']);

        if (!$lastUpdate) {
            throw new \RuntimeException('The last of getUpdates is empty!');
        }

        return new UpdateDTO($lastUpdate);
    }
}

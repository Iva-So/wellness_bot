<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use Symfony\Component\Notifier\ChatterInterface;

class MessageFromBotAPIWatcher
{
    public function __construct(
        readonly ChatterInterface $chatter,
        readonly GetLastMessageDTOFromBotAPI $getLastMessageDTOFromBotAPI
    ) {
    }

    public function run(): void
    {
        $lastUpdate = $this->getLastMessageDTOFromBotAPI->run();
        $lastMessageText = $lastUpdate->message->text;
        $textForSending = "Ответное сообщение на `$lastMessageText`";
        $sendMessageAction = new SendMessageToBotAPI(
            $lastUpdate->message->chat->id,
            $textForSending,
            $this->chatter
        );
        $sendMessageAction->run();
    }
}

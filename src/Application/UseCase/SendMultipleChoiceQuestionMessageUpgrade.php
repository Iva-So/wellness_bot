<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\BotConfigInterface;
use App\Application\Constants\KeyConfig;
use App\Domain\Contract\CacheRepositoryInterface as CacheRepository;
use App\Domain\Contract\HistoryRepositoryInterface as HistoryRepository;
use App\Domain\Contract\QuestionRepositoryInterface as QuestionRepository;
use App\Infrastructure\Log\LogInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\ReplyKeyboardMarkup;

class SendMultipleChoiceQuestionMessageUpgrade
{
    private BotApi $bot;
    private const DEFAULT_FIRST_SEQUENCE_QUESTION = '1';

    public function __construct(
        private readonly QuestionRepository $questionRepository,
        private readonly CacheRepository $cacheRepository,
        private readonly HistoryRepository $historyRepository,
        private readonly LogInterface $log,
        private readonly BotConfigInterface $botConfig
    ) {
        $this->bot = new BotApi($this->botConfig->getBotToken());
    }

    public function run(string $chatId): bool
    {
        $userSequenceQuestionKey = "user-chat:$chatId:sequenceQuestion";
        $sequenceQuestion = $this->cacheRepository->get($userSequenceQuestionKey) ?? self::DEFAULT_FIRST_SEQUENCE_QUESTION;
        $this->log->write("IndexQuestion:$sequenceQuestion", LogInterface::LOG_INFO);
        if ('1' == $sequenceQuestion) {
            $this->log->write("Refresh history by `$chatId`", LogInterface::LOG_INFO);
            $this->historyRepository->delete("user-chat:$chatId:wait"); // debug
            $this->historyRepository->delete("user-chat:$chatId:done");
        }

        $questions = $this->questionRepository->findAll();
        $questionsCount = count($questions);
        $this->log->write("CountQuestion:$questionsCount", LogInterface::LOG_INFO);

        if ($questionsCount < $sequenceQuestion) {
            $this->log->write('Research complete', LogInterface::LOG_INFO);
            $this->cacheRepository->delete($userSequenceQuestionKey);

            return false;
        }

        $currQuestion = $this->questionRepository->findOneBy(['sequence' => $sequenceQuestion]);

        $questionText = $currQuestion->getText();
        $this->log->write("Question text: $questionText", LogInterface::LOG_INFO);

        $sourceAnswers = $currQuestion->getAnswerChoice();
        $answers = $this->verticalConvertAnswers($sourceAnswers);
        $this->log->write('Answers:', LogInterface::LOG_INFO);
        $this->log->write(json_encode($answers), LogInterface::LOG_INFO);

        $keyboard = new ReplyKeyboardMarkup($answers, true, true);
        $this->bot->sendMessage($chatId, $questionText, null, false, null, $keyboard, true);

        $this->historyRepository->add("$chatId:wait", $sequenceQuestion, 'wait');
        $cacheLastQuestionKey = (new KeyConfig())->getUsersLastQuestionIdentifier($chatId);
        $this->cacheRepository->set($cacheLastQuestionKey, $sequenceQuestion);

        ++$sequenceQuestion;
        $this->cacheRepository->set($userSequenceQuestionKey, $sequenceQuestion);

        $this->log->write('Send multipleChoice question done', LogInterface::LOG_INFO);

        return true;
    }

    private function horizontalConvertAnswers(array $answers): array
    {
        return [array_values($answers)];
    }

    private function verticalConvertAnswers(array $answers): array
    {
        return array_map(static fn ($element) => [$element], array_values($answers));
    }
}

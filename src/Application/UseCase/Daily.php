<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Adapter\UpdateToShortMessageDTOAdapter;
use App\Domain\Contract\QueueRepositoryInterface;
use App\Infrastructure\Log\LogInterface;

class Daily
{
    public function __construct(
        private readonly QueueRepositoryInterface $queueRepository,
        private readonly LogInterface $log,
        private readonly GetLastMessageDTOFromBotAPI $getLastMessageDTOFromBotAPI
    ) {
    }

    public function run(): void
    {
        try {
            $lastUpdate = $this->getLastMessageDTOFromBotAPI->run();

            $shortUpdate = (new UpdateToShortMessageDTOAdapter())->adapt($lastUpdate);

            $messageOriginalText = $shortUpdate->getMessageText();
            $messageLog = "Send message `$messageOriginalText` to queue `daily`";
            $this->log->write($messageLog, LogInterface::LOG_DEBUG);
            $this->queueRepository->publish('daily', $messageOriginalText);
        } catch (\Exception $exception) {
            $this->log->write($exception->getMessage() . '!!!', LogInterface::LOG_ERROR);
        }
    }
}

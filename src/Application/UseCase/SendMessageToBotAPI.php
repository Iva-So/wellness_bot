<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use Symfony\Component\Notifier\Bridge\Telegram\TelegramOptions;
use Symfony\Component\Notifier\ChatterInterface;
use Symfony\Component\Notifier\Message\ChatMessage;

class SendMessageToBotAPI
{
    private int $chatId;
    private string $message;

    public function __construct(
        int $chatId,
        string $message,
        readonly ChatterInterface $chatter
    ) {
        $this->chatId = $chatId;
        $this->message = $message;
    }

    public function run(): void
    {
        $chatMessage = new ChatMessage($this->message);
        $telegramOptions = (new TelegramOptions())->chatId((string) $this->chatId);
        $chatMessage->options($telegramOptions);

        $this->chatter->send($chatMessage);
    }
}

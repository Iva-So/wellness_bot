<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\BotConfigInterface;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Exception;
use TelegramBot\Api\InvalidArgumentException;

class SendResponseMessageToBotAPI
{
    private BotApi $bot;

    /**
     * @throws \Exception
     */
    public function __construct(
        private readonly BotConfigInterface $botConfig,
    ) {
        $this->bot = new BotApi($this->botConfig->getBotToken());
    }

    /**
     * @throws Exception
     * @throws InvalidArgumentException
     */
    public function run(string $message, string $chatId): void
    {
        $this->bot->sendMessage($chatId, $message);
    }
}

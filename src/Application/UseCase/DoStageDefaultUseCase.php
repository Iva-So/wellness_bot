<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\DTO\ShortMessageDTOInterface;
use App\Infrastructure\Log\LogInterface;

readonly class DoStageDefaultUseCase implements UseCaseInterface
{
    public function __construct(
        private LogInterface $log
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $chatId = $lastShortUpdate->getChatId();
        $lastMessageText = $lastShortUpdate->getMessageText();

        $this->log->write("chatId:$chatId", LogInterface::LOG_INFO);
        $this->log->write("messageText:$lastMessageText", LogInterface::LOG_INFO);

        $this->log->write('Not implemented', LogInterface::LOG_ERROR);
    }
}

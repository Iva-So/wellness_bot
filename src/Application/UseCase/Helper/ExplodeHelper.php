<?php

declare(strict_types=1);

namespace App\Application\UseCase\Helper;

class ExplodeHelper
{
    public static function getSequenceQuestion(string $item): int
    {
        $arr = explode(',', $item);

        return (int) explode(':', $arr[0])[1];
    }

    public static function getAnswer(string $item): string
    {
        $arr = explode(',', $item);

        return explode(':', $arr[1])[1];
    }
}

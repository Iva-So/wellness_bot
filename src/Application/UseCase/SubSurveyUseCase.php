<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\KeyConfig;
use App\Application\DTO\ShortMessageDTOInterface;
use App\Application\Service\CustomerService;
use App\Application\UseCase\Recommendation\Send\SendResearchRecommendation;
use App\Application\UseCase\SendMultipleChoiceQuestionMessageUpgrade as SendMultipleQuestionMessage;
use App\Domain\Contract\AnswerRepositoryInterface;
use App\Domain\Contract\CacheRepositoryInterface;
use App\Domain\Contract\HistoryRepositoryInterface;
use App\Domain\Contract\QuestionRepositoryInterface;
use App\Domain\Entity\Answer;
use App\Domain\Entity\Customer;
use App\Infrastructure\Log\LogInterface;

readonly class SubSurveyUseCase implements UseCaseInterface
{
    public function __construct(
        private QuestionRepositoryInterface $questionRepository,
        private AnswerRepositoryInterface $answerRepository,
        private CacheRepositoryInterface $cacheRepository,
        private HistoryRepositoryInterface $historyRepository,
        private LogInterface $log,
        private SendMultipleQuestionMessage $sendMultipleChoiceQuestionMessage,
        private SendResearchRecommendation $sendRecommendation,
        private CustomerService $customerService
    ) {
    }

    public function run(ShortMessageDTOInterface $lastShortUpdate): void
    {
        $customerChatId = $lastShortUpdate->getChatId();
        $lastMessageText = $lastShortUpdate->getMessageText();
        $customerFirstName = $lastShortUpdate->getFirstName();

        $this->log->write("customerChatId:$customerChatId", LogInterface::LOG_INFO);
        $this->log->write("messageText:$lastMessageText", LogInterface::LOG_INFO);
        $this->log->write("customerFirstName:$customerFirstName", LogInterface::LOG_INFO);

        $customer = $this->customerService->save($customerFirstName, $customerChatId);

        $historyWaitKey = "$customerChatId:wait";
        $countHistoryWait = $this->historyRepository->count($historyWaitKey);
        $cacheLastQuestionKey = (new KeyConfig())->getUsersLastQuestionIdentifier($customerChatId);
        $lastQuestion = $this->cacheRepository->get($cacheLastQuestionKey);

        $this->log->write('key->value dump:', LogInterface::LOG_INFO);
        $this->log->write(
            json_encode([$historyWaitKey => $countHistoryWait, $cacheLastQuestionKey => $lastQuestion]),
            LogInterface::LOG_INFO
        );

        if ($lastQuestion) {
            $historyDoneKey = "$customerChatId:done";
            $msg = "update-history-repository: set `$historyDoneKey` to `lq=$lastQuestion;a=$lastMessageText`";
            $this->log->write($msg, LogInterface::LOG_INFO);
            $this->historyRepository->add($historyDoneKey, $lastQuestion, $lastMessageText);
        }

        $this->log->write("SendMultipleChoiceQuestion to `$customerChatId`  ...", LogInterface::LOG_INFO);

        $isContinue = $this->sendMultipleChoiceQuestionMessage->run($customerChatId);

        $this->log->write('Research done', LogInterface::LOG_INFO);

        if (!$isContinue) {
            $this->answersToRepository($customer);
        }
    }

    private function answersToRepository(Customer $customer): void
    {
        $chatId = $customer->getChatId();
        $this->log->write('answers to repository ...', LogInterface::LOG_INFO);
        $historyDoneKey = "$chatId:done";
        $msg = "`$historyDoneKey`::historyCount={$this->historyRepository->count($historyDoneKey)}";
        $this->log->write($msg, LogInterface::LOG_INFO);

        $dataAfterResearch = $this->historyRepository->extractAll($historyDoneKey);
        $this->sendRecommendation->run($dataAfterResearch, $chatId);
        foreach ($dataAfterResearch as $item) {
            [$sequenceQuestion, $answerText] = $this->getQuestionIdWithAnswerText($item);

            $question = $this->questionRepository->findOneBy(['sequence' => $sequenceQuestion]);

            $answer = new Answer();

            $answer->setQuestion($question);
            $answer->setText($answerText);
            $answer->setCreatedAtAutomatically();
            $answer->setUpdatedAtAutomatically();
            $answer->setCustomer($customer);

            $this->answerRepository->persist($answer);
            $msg = "Save answer `$answerText` for questionSeq `$sequenceQuestion`";
            $this->log->write($msg, LogInterface::LOG_INFO);
        }
        $this->answerRepository->flush();
        $this->log->write('Answers to repository done', LogInterface::LOG_INFO);
    }

    private function getQuestionIdWithAnswerText(string $textBinary): array
    {
        preg_match("/question:(\d+),answer:(.*?)(?=,|$)/", $textBinary, $matches);

        return [$matches[1], $matches[2]];
    }
}

<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Adapter\MatchMessageToUseCase as MatchMsgToUseCase;
use App\Application\Adapter\PreviosUpdateIdPortAdapter as PreUpdateIdPort;
use App\Application\Adapter\UpdatesToChatIdWithShortMessageDTOAdapter as UpdatesToChatIds;
use App\Application\DTO\ShortMessageDTOInterface;
use App\Infrastructure\Command\TelegramBotAPI\GetUpdatesQuery as BotGetUpdates;
use App\Infrastructure\Log\LogInterface as Log;

readonly class TelegramGetUpdates
{
    public function __construct(
        private Log $log,
        private MatchMsgToUseCase $messageToUseCase,
        private BotGetUpdates $fetchResult,
        private UpdatesToChatIds $adapter,
        private PreUpdateIdPort $preUpdateId
    ) {
    }

    public function run(): bool
    {
        $previousUpdateId = $this->preUpdateId->getPreviousUpdateId();
        $this->logDebug("* previousUpdateId: $previousUpdateId");

        $updatesResult = $this->fetchResult->getUpdates((int) $previousUpdateId);
        $resultCount = count($updatesResult);
        $this->logDebug("updates.result count: $resultCount");
        if (!$resultCount) {
            $this->log->write('updates are empty.', Log::LOG_INFO);

            return false;
        }

        $lastUpdateId = end($updatesResult)['update_id'];
        $this->logDebug("lastUpdateId: `$lastUpdateId`");

        $chatIds = $this->adapter->adapt($updatesResult);
        $this->logDebugChatIds($chatIds);

        /** @var ShortMessageDTOInterface $message */
        foreach ($chatIds as $chatId => $message) {
            $messageText = $message->getMessageText();
            $messageFirstName = $message->getFirstName();
            $msg = "message `$messageText` chat `$chatId` fName `$messageFirstName`";
            $this->log->write($msg, Log::LOG_INFO);
            $this->messageToUseCase->adaptAndRun($message);
            // "Your message `{$message->getMessageText()}` will calculate soon"
            // check sent the message
        }

        $this->preUpdateId->setPreviousUpdateId($lastUpdateId);
        $this->logDebug("** updated `lastUpdateId` to `$lastUpdateId`");

        $this->log->write('TelegramGetUpdates done.', Log::LOG_INFO);

        return true;
    }

    private function logDebugChatIds(array $chatIds): void
    {
        $this->log->write('** chatIds count: ' . count($chatIds), Log::LOG_DEBUG);
        $this->log->write('** chatIds ...', Log::LOG_DEBUG);
        $this->log->write(json_encode($chatIds), Log::LOG_DEBUG);
    }

    private function logDebug(string $message): void
    {
        $this->log->write($message, Log::LOG_DEBUG);
    }
}

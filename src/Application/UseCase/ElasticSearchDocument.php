<?php

declare(strict_types=1);

namespace App\Application\UseCase;

use App\Application\Constants\ElasticSearchConst;
use App\Application\Exceptions\ElasticSearchDocumentSearchException;
use App\Application\Factory\ElasticIndexFactory;
use App\Application\Factory\FactoryInterface;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Elastic\Elasticsearch\Response\Elasticsearch;
use Http\Promise\Promise;

class ElasticSearchDocument
{
    private ClientInterface $client;
    private FactoryInterface $factory;

    /**
     * @throws AuthenticationException
     */
    public function __construct()
    {
        $this->factory = new ElasticIndexFactory();
        $this->client = $this->factory->create();
    }

    /**
     * @throws ElasticSearchDocumentSearchException
     */
    public function searchDocument(string $productName = ''): Elasticsearch|Promise
    {
        $params = [
            'index' => ElasticSearchConst::ELASTIC_INDEX_NAME,
            'body' => [
                'query' => [
                    'match' => [
                        'name' => [
                            'query' => $productName,
                            'fuzziness' => 'auto',
                        ],
                    ],
                ],
            ],
        ];

        try {
            $response = $this->client->search($params);
            var_dump(__LINE__);
        } catch (ClientResponseException | ServerResponseException $exception) {
            var_dump(__LINE__);
            var_dump($exception->getMessage());
            throw new ElasticSearchDocumentSearchException();
        }

        return $response;
    }
}

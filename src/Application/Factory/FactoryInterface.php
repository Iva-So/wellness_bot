<?php

namespace App\Application\Factory;

use Elastic\Elasticsearch\ClientInterface as ElasticClientInterface;
use Predis\ClientInterface;

interface FactoryInterface
{
    public function create(): ClientInterface|ElasticClientInterface;
}

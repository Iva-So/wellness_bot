<?php

declare(strict_types=1);

namespace App\Application\Factory;

use Predis\Client;
use Predis\ClientInterface;

class RedisFactory implements FactoryInterface
{
    public function create(): ClientInterface
    {
        $redisHost = $_ENV['REDIS_HOST'] ?? '127.0.0.1';
        $redisPort = $_ENV['REDIS_PORT'] ?? '6379';
        $connectionParameters = "tcp://$redisHost:$redisPort";

        return new Client($connectionParameters);
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Factory;

use App\Application\Constants\ElasticSearchConst;
use Elastic\Elasticsearch\ClientBuilder;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;

class ElasticIndexFactory implements FactoryInterface
{
    /**
     * @throws AuthenticationException
     */
    public function create(): ClientInterface
    {
        return ClientBuilder::create()
            ->setHosts([$_ENV['ELASTIC_NODE_URL']])
            ->build();
    }
}

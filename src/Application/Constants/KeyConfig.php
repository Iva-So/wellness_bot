<?php

declare(strict_types=1);

namespace App\Application\Constants;

class KeyConfig
{
    public function getUsersLastQuestionIdentifier(string $usersChatId): string
    {
        return "user-chat:$usersChatId:lastQuestion";
    }

    public function getUsersCurrentCommandIdentifier(int $usersChatId): string
    {
        return "user-chat:$usersChatId:command";
    }

    public function getBotLastUpdateId(string $botUid): string
    {
        $uniqHash = hash('sha256', $botUid);
        $lastUpdateIdKey = substr($uniqHash, 0, 16);
        $lastUpdateIdKey = "app-hash:$lastUpdateIdKey:lastUpdateId";

        return $lastUpdateIdKey;
    }
}

<?php

namespace App\Application\Constants;

interface BotConfigInterface
{
    public function getApiUrl(): string;

    public function getBotToken(): string;
}

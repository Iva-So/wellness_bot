<?php

declare(strict_types=1);

namespace App\Application\Constants;

class ApiTelegramConst
{
    public const TELEGRAM_BOT = 'https://api.telegram.org/bot';
    public const GET_UPDATES = '/getUpdates';
}

<?php

declare(strict_types=1);

namespace App\Application\Constants;

class ElasticSearchConst
{
    public const ELASTIC_EXTERNAL_API_URL = 'https://api.api-ninjas.com/v1/nutrition?query=';
    public const ELASTIC_EXTERNAL_API_METHOD = 'GET';
    public const ELASTIC_EXTERNAL_API_HEADER = 'X-Api-Key';
    public const ELASTIC_INDEX_NAME = 'wellness_bot';
    public const ELASTIC_PRODUCT_DEFAULT_WEIGHT = '1000g ';
    public const ELASTIC_PRODUCT_DEFAULT_WEIGHT_INT = 1000;
    public const ELASTIC_PRODUCT_NAME_DEFAULT = '';
    public const ELASTIC_PRODUCT_WEIGHT_DEFAULT = -1;
}

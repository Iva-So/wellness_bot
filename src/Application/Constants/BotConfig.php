<?php

declare(strict_types=1);

namespace App\Application\Constants;

class BotConfig implements BotConfigInterface
{
    public function getApiUrl(): string
    {
        $hostWithToken = $this->getHostWithToken();

        $botMethod = $this->getBotMethod();

        return $hostWithToken . $botMethod;
    }

    public function getBotToken(): string
    {
        return $_ENV['BOT_TELEGRAM_TOKEN'];
    }

    private function getHostWithToken(): string
    {
        $host = $this->getBotHost();
        $token = $this->getBotToken();

        return sprintf('%s%s', $host, $token);
    }

    private function getBotHost(): string
    {
        return ApiTelegramConst::TELEGRAM_BOT;
    }

    private function getBotMethod(): string
    {
        return ApiTelegramConst::GET_UPDATES;
    }
}

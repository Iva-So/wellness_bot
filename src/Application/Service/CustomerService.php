<?php

namespace App\Application\Service;

use App\Domain\Contract\CustomerRepositoryInterface;
use App\Domain\Entity\Customer;

class CustomerService
{
    public function __construct(private readonly CustomerRepositoryInterface $customerRepository)
    {
    }

    public function save(string $firstName, string $chatId): Customer
    {
        $customer = $this->customerRepository->findOneBy(['chatId' => $chatId]);

        if (is_null($customer)) {
            $customer = new Customer();

            $customer->setFirstName($firstName);
            $customer->setChatId($chatId);
            $customer->setCreatedAtAutomatically();
            $customer->setUpdatedAtAutomatically();
            $this->customerRepository->persist($customer);
            $this->customerRepository->flush();
        }

        return $customer;
    }
}

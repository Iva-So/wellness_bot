<?php

declare(strict_types=1);

namespace App\Application\Service\Validator;

interface ValidatorInterface
{
    public function isValid(...$params): bool;
}

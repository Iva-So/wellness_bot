<?php

declare(strict_types=1);

namespace App\Application\Service\Validator;

use App\Application\Exceptions\ElasticSearchParamsValidationException;

class QueryParamsValidator implements ValidatorInterface
{
    /**
     * @throws ElasticSearchParamsValidationException
     */
    public function isValid(...$params): bool
    {
        foreach ($params as $param) {
            $type = gettype($param);

            switch ($type) {
                case 'double':
                    if ($param <= 0.0) {
                        throw new ElasticSearchParamsValidationException('Params must be greater than 0.0');
                    }
                    break;
                case 'string':
                    if ($param === '') {
                        throw new ElasticSearchParamsValidationException('Params must not be empty');
                    }
            }
        }

        return true;
    }
}

<?php

namespace App\Application\Service;

use App\Application\Constants\ElasticSearchConst;
use App\Application\DTO\ElasticSearchProductDTO;
use App\Application\Exceptions\ElasticSearchDocumentSaveException;
use App\Application\Exceptions\ElasticSearchDocumentSearchException;
use App\Application\Exceptions\ElasticSearchExternalApiException;
use App\Application\UseCase\ElasticSearchDocument;
use App\Domain\Contract\ElasticSearchRepositoryInterface;
use App\Infrastructure\Log\LogInterface;
use App\Infrastructure\Service\ExternalApiServiceInterface;
use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class ElasticSaveProductService
{
    public function __construct(
        private readonly ElasticSearchDocument $searchDocumentUseCase,
        private readonly ExternalApiServiceInterface $externalApiService,
        private readonly ElasticSearchRepositoryInterface $elasticSearchRepository,
        private readonly GetDtoByElasticData $dtoByElasticData,
        private readonly GetDtoByExternalApiData $dtoByExternalApiData,
        private readonly LogInterface $log
    ){}

    /**
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ClientExceptionInterface
     * @throws Exception
     */
    public function getProduct(string $productInfo): ElasticSearchProductDTO
    {
        $exploded = explode(',', $productInfo);
        $productName = $exploded[0] ?? ElasticSearchConst::ELASTIC_PRODUCT_NAME_DEFAULT;
        $productWeight = (float) ($exploded[1] ?? ElasticSearchConst::ELASTIC_PRODUCT_WEIGHT_DEFAULT);
        $this->log->write("Product name: `$productName` weight: `$productWeight`", LogInterface::LOG_INFO);

        try {
            $this->log->write('Search ' . $productName . ' by Elastic...', LogInterface::LOG_INFO);
            $resultElasticSearchDocument = $this->searchDocumentUseCase->searchDocument($productName)->asObject();
        } catch (ElasticSearchDocumentSearchException $exception) {
            $this->log->write('Searching ' . $productName . ' by Elastic failed. Message: ' . $exception->getMessage(), LogInterface::LOG_ERROR);
            throw new Exception($exception->getMessage());
        }

        if (empty($resultElasticSearchDocument->hits->hits)) {
            try {
                $this->log->write($productName . ' in Elastic no found. Search it by external API...', LogInterface::LOG_INFO);
                $dataByApi = json_decode($this->externalApiService->getProductData($productName)->getContent(), true);
            } catch (ElasticSearchExternalApiException $exception) {
                $this->log->write('Searching ' . $productName . ' by external API failed. Message: ' . $exception->getMessage(), LogInterface::LOG_ERROR);
                throw new Exception($exception->getMessage());
            }

            $this->log->write($productName . ' find by external API successful.', LogInterface::LOG_INFO);
            $productInfoByApi = array_shift($dataByApi);

            try {
                $this->log->write('Save ' . $productName . ' in Elastic...', LogInterface::LOG_INFO);
                $this->elasticSearchRepository->save($productInfoByApi);
            } catch (ElasticSearchDocumentSaveException $exception) {
                $this->log->write('Saving ' . $productName . ' in Elastic failed. Message: ' . $exception->getMessage(), LogInterface::LOG_ERROR);
                throw new Exception($exception->getMessage());
            }

            $this->log->write($productName . ' save in Elastic successful.', LogInterface::LOG_INFO);

            return $this->dtoByExternalApiData->getElasticSearchProductDto($productInfoByApi, $productWeight);
        }

        $this->log->write('Search ' . $productName . ' by Elastic successful', LogInterface::LOG_INFO);
        $resultDocument = array_shift($resultElasticSearchDocument->hits->hits)->_source;
        return $this->dtoByElasticData->getElasticSearchProductDto($resultDocument, $productWeight);
    }
}

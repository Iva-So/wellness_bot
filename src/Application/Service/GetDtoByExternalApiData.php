<?php

namespace App\Application\Service;

use App\Application\Constants\ElasticSearchConst;
use App\Application\DTO\ElasticSearchProductDTO;

class GetDtoByExternalApiData
{
    public function getElasticSearchProductDto(array $data, float $productWeight): ElasticSearchProductDTO
    {
        return new ElasticSearchProductDTO(
            (string)$data['name'],
            (float)$data['calories'] / ElasticSearchConst::ELASTIC_PRODUCT_DEFAULT_WEIGHT_INT * $productWeight,
            (float)$data['fat_total_g'] / ElasticSearchConst::ELASTIC_PRODUCT_DEFAULT_WEIGHT_INT * $productWeight,
            (float)$data['protein_g'] / ElasticSearchConst::ELASTIC_PRODUCT_DEFAULT_WEIGHT_INT * $productWeight,
            (float)$data['carbohydrates_total_g'] / ElasticSearchConst::ELASTIC_PRODUCT_DEFAULT_WEIGHT_INT * $productWeight,
            $productWeight
        );
    }
}

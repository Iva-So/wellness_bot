<?php

declare(strict_types=1);

namespace App\Application\DTO;

use App\Domain\Entity\Customer;
use App\Domain\Entity\Recommendation;

class RecommendationDTO
{
    public ?Customer $customer = null;
    public ?Recommendation $lastRecommendation = null;
    public ?string $createdAtWeeklyWeight = null;
    public ?int $dailyCalorie = null;
    public ?float $customerWeight = null;
    public ?float $protein = null;
    public ?float $fat = null;
    public ?float $carbohydrate = null;
}

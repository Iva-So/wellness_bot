<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class ShortMessageDTO implements ShortMessageDTOInterface
{
    private string $chatId;
    private string $message;
    private string $firstName;

    public function __construct(
        string $chatId,
        string $message,
        string $firstName = ''
    ) {
        $this->chatId = $chatId;
        $this->message = $message;
        $this->firstName = $firstName;
    }

    public function getChatId(): string
    {
        return $this->chatId;
    }

    public function getMessageText(): string
    {
        return $this->message;
    }

    public function getFirstName(): string
    {
        return $this->firstName;
    }
}

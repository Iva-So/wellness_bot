<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class UpdateMessageDTO
{
    public int $messageId;
    public FromMessageUpdateDTO $from;
    public ChatMessageUpdateDTO $chat;
    public int $date;
    public string $text;

    public function __construct(array $message)
    {
        $this->messageId = $message['message_id'];
        $this->from = new FromMessageUpdateDTO($message['from']);
        $this->chat = new ChatMessageUpdateDTO($message['chat']);
        $this->date = $message['date'];
        $this->text = $message['text'];
    }
}

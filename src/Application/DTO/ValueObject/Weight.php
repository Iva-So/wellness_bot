<?php

declare(strict_types=1);

namespace App\Application\DTO\ValueObject;

use App\Application\Exceptions\WeightException;

class Weight
{
    private string $weight;

    /**
     * @throws WeightException
     */
    public function __construct(string $weight)
    {
        $this->assertWeightIsValid($weight);
        $this->weight = $weight;
    }

    public function getWeight(): int
    {
        return (int) $this->weight;
    }

    /**
     * @throws WeightException
     */
    private function assertWeightIsValid(string $weight): void
    {
        if (1 != preg_match("/^\d+$/", $weight)) {
            throw new WeightException("'weight' is not valid");
        }
    }
}

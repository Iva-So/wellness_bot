<?php

declare(strict_types=1);

namespace App\Application\DTO\ValueObject;

use App\Application\Exceptions\AgeException;

class Age
{
    private string $age;

    /**
     * @throws AgeException
     */
    public function __construct(string $age)
    {
        $this->assertAgeIsValid($age);
        $this->age = $age;
    }

    public function getAge(): int
    {
        return (int) $this->age;
    }

    /**
     * @throws AgeException
     */
    private function assertAgeIsValid(string $age): void
    {
        if (1 != preg_match("/^\d+$/", $age)) {
            throw new AgeException("'age' is not valid");
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Application\DTO\ValueObject;

use App\Application\Exceptions\HeightException;

class Height
{
    private string $height;

    /**
     * @throws HeightException
     */
    public function __construct(string $height)
    {
        $this->assertHeightIsValid($height);
        $this->height = $height;
    }

    public function getHeight(): int
    {
        return (int) $this->height;
    }

    /**
     * @throws HeightException
     */
    private function assertHeightIsValid(string $height): void
    {
        if (1 != preg_match("/^\d+$/", $height)) {
            throw new HeightException("'height' is not valid");
        }
    }
}

<?php

declare(strict_types=1);

namespace App\Application\DTO\ValueObject;

use App\Application\Enum\Gender as GenderEnum;
use App\Application\Exceptions\GenderException;

class Gender
{
    private string $gender;

    /**
     * @throws GenderException()
     */
    public function __construct(string $gender)
    {
        $this->assertGenderIsValid($gender);
        $this->gender = $gender;
    }

    public function getGender(): string
    {
        return $this->gender;
    }

    /**
     * @throws GenderException()
     */
    private function assertGenderIsValid(string $gender): void
    {
        if (!in_array($gender, [GenderEnum::MALE->value, GenderEnum::FEMALE->value])) {
            throw new GenderException("'gender' is not valid");
        }
    }
}

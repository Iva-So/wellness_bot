<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class UpdateDTO
{
    public int $updateId;
    public UpdateMessageDTO $message;

    public function __construct(array $update)
    {
        $this->updateId = $update['update_id'];
        $this->message = new UpdateMessageDTO($update['message']);
    }
}

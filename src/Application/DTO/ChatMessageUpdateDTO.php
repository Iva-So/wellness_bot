<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class ChatMessageUpdateDTO
{
    public int $id;
    public string $firstName;
    public string $type;

    public function __construct(array $chat)
    {
        $this->id = $chat['id'];
        $this->firstName = $chat['first_name'];
        $this->type = $chat['type'];
    }
}

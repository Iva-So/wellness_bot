<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class ElasticSearchProductDTO
{
    public function __construct(
        private readonly string $name,
        private readonly float $calories,
        private readonly float $fatTotalGram,
        private readonly float $proteinGram,
        private readonly float $carbohydratesTotalGram,
        private readonly float $servingSizeGram
    ) {
    }

    /**
     * @return float
     */
    public function getFatTotalGram(): float
    {
        return $this->fatTotalGram;
    }

    /**
     * @return float
     */
    public function getProteinGram(): float
    {
        return $this->proteinGram;
    }

    /**
     * @return float
     */
    public function getCarbohydratesTotalGram(): float
    {
        return $this->carbohydratesTotalGram;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getCalories(): float
    {
        return $this->calories;
    }

    /**
     * @return float
     */
    public function getServingSizeGram(): float
    {
        return $this->servingSizeGram;
    }
}

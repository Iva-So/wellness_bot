<?php

declare(strict_types=1);

namespace App\Application\DTO;

use App\Application\DTO\ValueObject\Age;
use App\Application\DTO\ValueObject\Gender;
use App\Application\DTO\ValueObject\Height;
use App\Application\DTO\ValueObject\Weight;

class CustomerParamDto
{
    public ?Age $age = null;
    public ?Weight $weight = null;
    public ?Height $height = null;
    public ?Gender $gender = null;
}

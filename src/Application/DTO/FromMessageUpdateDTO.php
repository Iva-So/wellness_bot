<?php

declare(strict_types=1);

namespace App\Application\DTO;

readonly class FromMessageUpdateDTO
{
    public int $id;
    public bool $isBot;
    public string $firstName;
    public string $languageCode;

    public function __construct(array $from)
    {
        $this->id = $from['id'];
        $this->isBot = $from['is_bot'];
        $this->firstName = $from['first_name'];
        $this->languageCode = $from['language_code'];
    }
}

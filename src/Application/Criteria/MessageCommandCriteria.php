<?php

declare(strict_types=1);

namespace App\Application\Criteria;

class MessageCommandCriteria
{
    public function isCommand(string $text): bool
    {
        return $this->isMessageCommand($text);
    }

    private function isMessageCommand(string $messageText): bool
    {
        return (strlen($messageText) > 1) && str_starts_with($messageText, '/');
    }
}

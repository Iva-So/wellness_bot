<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchIndexCreateException extends Exception
{
}

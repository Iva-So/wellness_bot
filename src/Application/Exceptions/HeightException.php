<?php

declare(strict_types=1);

namespace App\Application\Exceptions;

class HeightException extends \Exception
{
}

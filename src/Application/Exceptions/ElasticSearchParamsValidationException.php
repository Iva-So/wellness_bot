<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchParamsValidationException extends Exception
{
}

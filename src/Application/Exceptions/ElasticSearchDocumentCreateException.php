<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchDocumentCreateException extends Exception
{
}

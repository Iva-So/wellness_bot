<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchDocumentSaveException extends Exception
{
}

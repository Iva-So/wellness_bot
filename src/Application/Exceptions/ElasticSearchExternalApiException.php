<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchExternalApiException extends Exception
{
}

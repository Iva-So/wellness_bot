<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchIndexDeleteException extends Exception
{
}

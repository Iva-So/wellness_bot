<?php

namespace App\Application\Exceptions;

use Exception;

class ElasticSearchDocumentSearchException extends Exception
{
}

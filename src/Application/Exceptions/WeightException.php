<?php

declare(strict_types=1);

namespace App\Application\Exceptions;

class WeightException extends \Exception
{
}

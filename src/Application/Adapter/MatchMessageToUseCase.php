<?php

declare(strict_types=1);

namespace App\Application\Adapter;

use App\Application\Constants\KeyConfig;
use App\Application\Criteria\MessageCommandCriteria as CommandOrMessageOnly;
use App\Application\DTO\ShortMessageDTOInterface as MsgDTOInterface;
use App\Application\Enum\MenuCommand;
use App\Application\UseCase\DoStageRecommendationUseCase;
use App\Application\UseCase\DoStageStartUseCase;
use App\Application\UseCase\DoStage1SurveyUseCase;
use App\Application\UseCase\DoStage2DailyUseCase;
use App\Application\UseCase\DoStageDefaultUseCase;
use App\Application\UseCase\DoStageNutritionUseCase;
use App\Application\UseCase\DoStageTrainingUseCase;
use App\Application\UseCase\DoStageWeeklyUseCase;
use App\Application\UseCase\SendResponseMessageToBotAPI as SendResponseMessage;
use App\Application\UseCase\UseCaseInterface;
use App\Domain\Contract\CacheRepositoryInterface as CacheRepository;
use App\Domain\Contract\CommandRepositoryInterface as CommandRepository;
use App\Domain\Entity\Command;
use App\Infrastructure\Log\LogInterface as Log;
use RuntimeException;

class MatchMessageToUseCase
{
    public function __construct(
        readonly private CacheRepository         $cacheRepository,
        readonly private Log                     $log,
        readonly private CommandRepository       $commandRepository,
        readonly private SendResponseMessage     $sendResponse,
        readonly private DoStage1SurveyUseCase   $doStage1Survey,
        readonly private DoStage2DailyUseCase    $doStage2DailyUseCase,
        readonly private DoStageTrainingUseCase  $doStageTrainingUseCase,
        readonly private DoStageNutritionUseCase $doStageNutritionUseCase,
        readonly private DoStageDefaultUseCase   $doStageDefaultUseCase,
        readonly private DoStageStartUseCase     $doStageStartUseCase,
        readonly private KeyConfig               $keyConfig,
        readonly private CommandOrMessageOnly    $messageAdapter,
        readonly private DoStageRecommendationUseCase $doStageRecommendationUseCase,
        readonly private DoStageWeeklyUseCase $doStageWeeklyUseCase
    ) {
    }

    public function adaptAndRun(MsgDTOInterface $message): void
    {
        $chatId = $message->getChatId();
        $cacheCommandKey = $this->keyConfig->getUsersCurrentCommandIdentifier((int) $chatId);

        $messageText = $message->getMessageText();
        if ($this->messageAdapter->isCommand($messageText)) {
            $msg = "message `$messageText` chat $chatId IS command";
            $this->log->write($msg, Log::LOG_INFO);

            $command = $this->getCommandFromMessage($messageText);

            $response = $this->getResponseByCommand($command, $message);
            $this->sendResponse->run($response, $chatId);

            $msg = "cache Set `$cacheCommandKey` to `{$command->getName()}`";
            $this->log->write($msg, Log::LOG_INFO);
            $this->cacheRepository->set($cacheCommandKey, $command->getName());

            $useCase = $this->getUseCaseAfterTheCommand($command);
            $useCase->run($message);

            return;
        }

        $currentCommandText = $this->cacheRepository->get($cacheCommandKey) ?? '';
        $msg = "cache Get `$cacheCommandKey` val `$currentCommandText`";
        $this->log->write($msg, Log::LOG_INFO);

        $command = $this->getCommandFromMessage($currentCommandText);
        $useCase = $this->getUseCaseAfterTheCommand($command);
        $useCase->run($message);
    }

    private function getResponseByCommand(Command $command, MsgDTOInterface $msgDTO): string
    {
        $msg = "command `{$command->getName()}` on line: " . __LINE__;
        $this->log->write($msg, Log::LOG_DEBUG);

        return match ($command->getName()) {
            MenuCommand::START->value => "{$msgDTO->getFirstName()}! {$command->getText()}",
            default => $command->getText()
        };
    }

    private function getCommandFromMessage(string $messageText): Command
    {
        if (!MenuCommand::tryFrom($messageText)) {
            $msg = "try - Fail - command `$messageText`";
            $this->log->write($msg, Log::LOG_ERROR);
            throw new RuntimeException($msg);
        }

        $msg = "messageText: $messageText on line: " . __LINE__;
        $this->log->write($msg, Log::LOG_DEBUG);

        if ($command = $this->commandRepository->findOneBy(['name' => $messageText])) {
            return $command;
        }

        $msg = "command `$messageText` not found";
        $this->log->write($msg, Log::LOG_ERROR);
        throw new RuntimeException($msg);
    }

    private function getUseCaseAfterTheCommand(Command $command): UseCaseInterface
    {
        $msg = "commandName `{$command->getName()}` on line: " . __LINE__;
        $this->log->write($msg, Log::LOG_DEBUG);

        return match ($command->getName()) {
            MenuCommand::SURVEY->value => $this->doStage1Survey,
            MenuCommand::DAILY->value => $this->doStage2DailyUseCase,

            MenuCommand::TRAINING->value => $this->doStageTrainingUseCase,
            MenuCommand::NUTRITION->value => $this->doStageNutritionUseCase,
            MenuCommand::START->value => $this->doStageStartUseCase,

            MenuCommand::RECOMMENDATION->value => $this->doStageRecommendationUseCase,
            MenuCommand::WEIGHT->value => $this->doStageWeeklyUseCase,

            default => $this->doStageDefaultUseCase
        };
    }
}

<?php

declare(strict_types=1);

namespace App\Application\Adapter;

use App\Application\Constants\BotConfig;
use App\Application\Constants\KeyConfig;
use App\Domain\Contract\CacheRepositoryInterface;

class PreviosUpdateIdPortAdapter
{
    private string $previousUpdateIdKey;

    public function __construct(
        private readonly CacheRepositoryInterface $cacheRepository,
        private readonly KeyConfig $keyConfig,
        private readonly BotConfig $botConfig,
    ) {
        $this->previousUpdateIdKey = $this->keyConfig->getBotLastUpdateId($this->botConfig->getBotToken());
    }

    public function getPreviousUpdateId(): string
    {
        return $this->cacheRepository->get($this->previousUpdateIdKey) ?? '0';
    }

    public function setPreviousUpdateId(int $lastUpdateId): void
    {
        $this->cacheRepository->set($this->previousUpdateIdKey, $lastUpdateId);
    }
}

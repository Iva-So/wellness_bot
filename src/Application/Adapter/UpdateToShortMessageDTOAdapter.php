<?php

declare(strict_types=1);

namespace App\Application\Adapter;

use App\Application\DTO\ShortMessageDTO;
use App\Application\DTO\UpdateDTO;

class UpdateToShortMessageDTOAdapter
{
    public function adapt(UpdateDTO $lastUpdate): ShortMessageDTO
    {
        return new ShortMessageDTO(
            (string)$lastUpdate->message->chat->id,
            $lastUpdate->message->text
        );
    }
}

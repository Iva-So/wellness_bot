<?php

declare(strict_types=1);

namespace App\Application\Adapter;

use App\Application\DTO\ShortMessageDTO;
use ArrayIterator;

class UpdatesToChatIdWithShortMessageDTOAdapter
{
    public function adapt(array $updates): array
    {
        $chatIds = [];
        foreach ($updates as $update) {
            if (!isset($update['message'])) {
                continue;
            }

            /** @var array $message */
            $message = $update['message'];
            $chatId = (string) $message['chat']['id'];
            $firstName = $message['chat']['first_name'] ?? '';
            $messageText = (string) $message['text'];
            $chatIds[$chatId] = new ShortMessageDTO(
                $chatId,
                $messageText,
                $firstName
            );
        }

        return $chatIds;
    }

    public function adaptToIterator(array $chatIds): ArrayIterator
    {
        return new ArrayIterator($chatIds);
    }
}

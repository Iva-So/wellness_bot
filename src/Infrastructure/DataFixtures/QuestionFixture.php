<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Application\Enum\Gender;
use App\Domain\Entity\Question;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class QuestionFixture extends Fixture
{
    private array $questions = [
        [
            'text' => 'Укажите ваш пол.',
            'status' => Question::COMMON_STATUS,
            'type' => Question::MULTI_TYPE,
            'sequence' => 1,
            'answer_choice' => [Gender::MALE->value => 'Мужчина', Gender::FEMALE->value => 'Женщина'],
        ],
        [
            'text' => 'Сколько вам лет?',
            'status' => Question::COMMON_STATUS,
            'type' => Question::SINGLE_TYPE,
            'sequence' => 2,
        ],
        [
            'text' => 'Какая у вас профессия?',
            'status' => Question::COMMON_STATUS,
            'type' => Question::SINGLE_TYPE,
            'sequence' => 3,
        ],
        [
            'text' => 'Какой у вас рост? Напишите ваш рост в сантиметрах, например: 175',
            'status' => Question::COMMON_STATUS,
            'type' => Question::SINGLE_TYPE,
            'sequence' => 4,
        ],
        [
            'text' => 'Какой у вас вес? Напишите ваш вес в килограммах, например: 50',
            'status' => Question::COMMON_STATUS,
            'type' => Question::SINGLE_TYPE,
            'sequence' => 5,
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->questions as $question) {
            $q = new Question();
            $q->setText($question['text']);
            $q->setStatus($question['status']);
            $q->setType($question['type']);
            $q->setSequence($question['sequence']);
            $q->setCreatedAtAutomatically();
            $q->setUpdatedAtAutomatically();

            if (isset($question['answer_choice'])) {
                $q->setAnswerChoice($question['answer_choice']);
            }

            $manager->persist($q);
        }
        $manager->flush();
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Domain\Entity\Customer;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CustomerFixture extends Fixture
{
    private array $customers = [
        [
            'firstName' => 'Alex',
        ],
    ];

    /**
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        foreach ($this->customers as $customer) {
            $c = new Customer();
            $c->setFirstName($customer['firstName']);
            $c->setChatId((string) random_int(1, 10000));
            $c->setCreatedAtAutomatically();
            $c->setUpdatedAtAutomatically();

            $manager->persist($c);
        }

        $manager->flush();
    }
}

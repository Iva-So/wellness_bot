<?php

declare(strict_types=1);

namespace App\Infrastructure\DataFixtures;

use App\Application\Enum\MenuCommand;
use App\Domain\Entity\Command as EntityCommand;
use App\Domain\Entity\Resource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CommandFixture extends Fixture
{
    private array $commands = [
        [
            'name' => MenuCommand::TRAINING,
            'text' => 'Интересные рекомендации по тренировкам.',
        ],
        [
            'name' => MenuCommand::NUTRITION,
            'text' => 'Интересные рекомендации по питанию.',
        ],
        [
            'name' => MenuCommand::START,
            'text' => 'Рады вас видеть! Я - ваш помощник в улучшении вашего тела. Для начала проведем небольшой опрос. Для этого нажмите в меню пункт "Анкетирование".',
        ],
        [
            'name' => MenuCommand::HELP,
            'text' => 'Я умею проводить анкетирование, после которого даю рекомендации для коррекции вашего веса - нажмите в меню «Анкетирование», мониторинг питания, где ежедневно получаю от вас съеденные продукты и считаю вашу дневную калорийность и баланс бжу - нажмите в меню «Мониторинг питания», мониторинг веса, где от вас получаю еженедельный вес и провожу анализ изменений - нажмите в меню «Мониторинг веса», давать рекомендации на основе анкетирования, мониторинга питания, мониторинга веса - нажмите в меню «Рекомендации»,  также имею полезные статьи и ролики для погружения в мир правильного питания и тренировок - нажмите в меню «Тренировки», «Питание».',
        ],
        [
            'name' => MenuCommand::SURVEY,
            'text' => 'Ответьте на следующие вопросы.',
        ],
        [
            'name' => MenuCommand::DAILY,
            'text' => 'Отправьте продукты, которые сегодня съели.',
        ],
        [
            'name' => MenuCommand::RECOMMENDATION,
            'text' => 'Формируем для вас рекомендации на основе ваших ежедневных данных.',
        ],
        [
            'name' => MenuCommand::WEIGHT,
            'text' => 'Какой у вас сейчас вес тела? Напишите ваш вес в килограммах, например: 50',
        ],
    ];

    private array $trainingResources = [
        [
            'description' => 'Рабочая ТАБАТА',
            'link' => 'https://www.youtube.com/watch?v=vYR9NhHU43U',
        ],
        [
            'description' => 'Как убрать живот за 4 минуты?',
            'link' => 'https://www.youtube.com/watch?v=zT4NJC5fzWg',
        ],
        [
            'description' => 'Как УЛУЧШИТЬ ФОРМУ',
            'link' => 'https://www.youtube.com/watch?v=IN8Dln1JfLQ',
        ],
    ];

    private array $nutritionResources = [
        [
            'description' => 'Что такое РПП?',
            'link' => 'https://www.youtube.com/watch?v=yrT5qJq_Ook&list=PLo5QwAM6HNwJZFeGM_KxwJiTS4A7Qudaj',
        ],
        [
            'description' => 'РПП: Причины',
            'link' => 'https://www.youtube.com/watch?v=HUj5ZX5nqYI&list=PLo5QwAM6HNwJZFeGM_KxwJiTS4A7Qudaj&index=6',
        ],
        [
            'description' => 'Интуитивное питание: МИФЫ и ФАКТЫ.',
            'link' => 'https://www.youtube.com/watch?v=2TEe3UcorRE&list=PLo5QwAM6HNwJZFeGM_KxwJiTS4A7Qudaj&index=4',
        ],
    ];

    public function load(ObjectManager $manager): void
    {
        foreach ($this->commands as $command) {
            $c = new EntityCommand();
            $c->setName($command['name']->value);
            $c->setText($command['text']);
            $c->setCreatedAtAutomatically();
            $c->setUpdatedAtAutomatically();

            $manager->persist($c);

            if (MenuCommand::TRAINING === $command['name']) {
                $this->loadTrainingResources($manager, $c);
            }

            if (MenuCommand::NUTRITION === $command['name']) {
                $this->loadNutritionResources($manager, $c);
            }
        }
        $manager->flush();
    }

    public function loadTrainingResources(ObjectManager $manager, EntityCommand $c): void
    {
        foreach ($this->trainingResources as $resource) {
            $r = new Resource();
            $r->setCommand($c);
            $r->setDescription($resource['description']);
            $r->setLink($resource['link']);
            $r->setCreatedAtAutomatically();
            $r->setUpdatedAtAutomatically();

            $manager->persist($r);
        }
    }

    public function loadNutritionResources(ObjectManager $manager, EntityCommand $c): void
    {
        foreach ($this->nutritionResources as $resource) {
            $r = new Resource();
            $r->setCommand($c);
            $r->setDescription($resource['description']);
            $r->setLink($resource['link']);
            $r->setCreatedAtAutomatically();
            $r->setUpdatedAtAutomatically();

            $manager->persist($r);
        }
    }
}

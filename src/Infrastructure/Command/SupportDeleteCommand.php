<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'supp:delete',
    description: 'Support: Delete keys',
)]
class SupportDeleteCommand extends Command
{
    private FactoryInterface $factory;

    public function __construct()
    {
        parent::__construct();

        $this->factory = new RedisFactory();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('pattern', InputArgument::REQUIRED, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $pattern = $input->getArgument('pattern');
        $client = $this->factory->create();

        if ($keys = $client->keys($pattern)) {
            $client->del($keys);
            $io->success('Delete');
        }

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'supp:list-vals',
    description: 'Support: List values, always all keys with postfix `done` or `wait`',
)]
class SupportListValuesCommand extends Command
{
    private FactoryInterface $factory;

    public function __construct()
    {
        parent::__construct();

        $this->factory = new RedisFactory();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $arg = '*';
        $client = $this->factory->create();
        $keys = $client->keys($arg);
        echo PHP_EOL;
        foreach ($keys as $key) {
            $postFix = substr($key, -4);
            if (in_array($postFix, ['done', 'wait'])) {
                $val = $client->lrange($key, 0, -1);
                echo "key: '$key' with data:";
                echo PHP_EOL;

                echo var_export($val, true);
                echo PHP_EOL;
                echo PHP_EOL;
            }
        }

        $io->success('Command `supp:list-values` done');

        return Command::SUCCESS;
    }
}

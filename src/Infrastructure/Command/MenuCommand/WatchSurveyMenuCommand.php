<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\MenuCommand;

use App\Application\UseCase\MenuCommand\AnswerSurveyCommand;
use App\Infrastructure\Log\LogInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

#[AsCommand(
    name: 'menu:survey',
    description: 'Get survey bot command from user',
)]
class WatchSurveyMenuCommand extends Command
{
    public function __construct(
        private readonly AnswerSurveyCommand $command,
        private readonly LogInterface $log
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->command->run();
            $io->success('Success');
        } catch (Throwable $e) {
            $this->log->write($e->getMessage(), LogInterface::LOG_ERROR);
            $io->warning($e->getMessage());
        }

        return Command::SUCCESS;
    }
}

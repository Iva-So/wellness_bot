<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\MenuCommand;

use App\Application\UseCase\MenuCommand\AnswerTrainingCommand;
use App\Infrastructure\Log\LogInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

#[AsCommand(
    name: 'menu:training',
    description: 'Get training bot command from user',
)]
class WatchTrainingMenuCommand extends Command
{
    public function __construct(
        private readonly AnswerTrainingCommand $command,
        private readonly LogInterface $log
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->command->run();
            $io->success('Success');
        } catch (Throwable $e) {
            $this->log->write($e->getMessage(), LogInterface::LOG_ERROR);
            $io->warning($e->getMessage());
        }

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\UseCase\ChatWithLastMessageBotAPI;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'watch:chat-with-message',
    description: 'Watch chat with message',
)]
class WatchChatWithMessageCommand extends Command
{
    public function __construct(private readonly ChatWithLastMessageBotAPI $action)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->action->run();

        $io->success('Command `watch:chat-with-message` done.');

        return Command::SUCCESS;
    }
}

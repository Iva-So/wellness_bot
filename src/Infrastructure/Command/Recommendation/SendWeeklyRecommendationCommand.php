<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\Recommendation;

use App\Application\UseCase\Recommendation\Send\SendWeeklyRecommendation;
use App\Infrastructure\Log\LogInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Throwable;

#[AsCommand(
    name: 'recommendation:weekly-send',
    description: 'Send weekly recommendation',
)]
class SendWeeklyRecommendationCommand extends Command
{
    public function __construct(
        private readonly SendWeeklyRecommendation $send,
        private readonly LogInterface $log
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $this->send->run();
            $io->success('Success');
        } catch (Throwable $e) {
            $this->log->write($e->getMessage(), LogInterface::LOG_ERROR);
            $io->warning($e->getMessage());
        }

        return Command::SUCCESS;
    }
}

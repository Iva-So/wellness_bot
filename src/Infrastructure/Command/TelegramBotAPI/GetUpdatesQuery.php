<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\TelegramBotAPI;

use App\Application\Constants\BotConfigInterface;
use App\Infrastructure\Log\LogInterface;
use App\Infrastructure\Log\LogInterface as Log;
use RuntimeException;

class GetUpdatesQuery
{
    public function __construct(
        private LogInterface $log,
        private BotConfigInterface $botConfig
    ) {
    }

    public function getUpdates(int $offset, int $limit = 100): array
    {
        $url = $this->makeUrl($offset);
        $this->log->write("** Offset: $offset", Log::LOG_DEBUG);

        try {
            if ($stream = fopen($url, 'rb')) {
                $data = stream_get_contents($stream);
                fclose($stream);

                return json_decode($data, true)['result'] ?? [];
            }
        } catch (RuntimeException $exception) {
            $msg = "Url `$url` is unreachable . `{$exception->getMessage()}`";
            $this->log->write($msg, Log::LOG_ERROR);
            throw new \RuntimeException($msg);
        }
    }

    private function makeUrl(int $offset): string
    {
        $query = http_build_query(['offset' => $offset + 1]);
        $url = $this->botConfig->getApiUrl();

        return $offset ? "$url?$query" : $url;
    }
}

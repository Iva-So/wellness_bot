<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\TelegramBotAPI;

use App\Infrastructure\Repository\CommandRepository;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use TelegramBot\Api\BotApi;
use TelegramBot\Api\Types\BotCommand;

#[AsCommand(
    name: 'telegram:menu-init',
    description: 'Initialize menu for your telegram bot',
)]
class TelegramMenuInitCommand extends Command
{
    public function __construct(
        private readonly CommandRepository $commandRepository
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $botToken = $_ENV['BOT_TELEGRAM_TOKEN'];

        $botCommands = [];
        $all = $this->commandRepository->findAll();
        /** @var \App\Domain\Entity\Command $command */
        foreach ($all as $command) {
            $botCommand = new BotCommand();
            $botCommand->setCommand($command->getName());
            $botCommand->setDescription($command->getText());
            $botCommands[] = $botCommand;
        }

        $telegram = new BotApi($botToken);
        $response = $telegram->setMyCommands($botCommands);

        if ($response) {
            $msg = 'Bot commands set successfully';
            $io->success($msg);
        } else {
            $msg = 'Failed to set bot commands: ' . $response->getDescription();
            $io->error($msg);
        }

        return Command::SUCCESS;
    }
}

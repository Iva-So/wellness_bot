<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\TelegramBotAPI;

use App\Infrastructure\Log\FileLog;
use App\Infrastructure\Log\LogInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'telegram:cron-mock',
    description: 'Add a short description for your command',
)]
class TelegramCronMockCommand extends Command
{
    public function __construct(private FileLog $log)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $message = 'Started';

        $cmd = '/srv/www/bin/console telegram:get-updates';
        $cmd = escapeshellcmd($cmd);
        shell_exec($cmd);

        $message = sprintf(
            '[%s] %s',
            date('Y-m-d H:i:s'),
            $message
        );
        $this->log->write($message, LogInterface::LOG_INFO);

        return Command::SUCCESS;
    }
}

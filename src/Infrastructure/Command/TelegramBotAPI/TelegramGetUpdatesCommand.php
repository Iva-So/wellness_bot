<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\TelegramBotAPI;

use App\Application\UseCase\TelegramGetUpdates;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'telegram:get-updates',
    description: 'Telegram bot, user, sync command',
)]
class TelegramGetUpdatesCommand extends Command
{
    public function __construct(
        readonly TelegramGetUpdates $action
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->action->run();

        $io->success('Command `telegram:get-updates` done.');

        return Command::SUCCESS;
    }
}

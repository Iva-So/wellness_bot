<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\UseCase\DataWeeklyMessageQueue;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'watch:data-weekly-message-queue',
    description: 'Watching data weekly message from queue',
)]
class WatchDataWeeklyMessageQueueCommand extends Command
{
    public function __construct(
        private readonly ?DataWeeklyMessageQueue $weeklyMessageQueue = null
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->weeklyMessageQueue->run();

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'supp:set',
    description: 'Support: Set to Key your Value',
)]
class SupportSetCommand extends Command
{
    private FactoryInterface $factory;

    public function __construct()
    {
        parent::__construct();

        $this->factory = new RedisFactory();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::REQUIRED, 'Argument1 description')
            ->addArgument('arg2', InputArgument::OPTIONAL, 'Argument2 description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg1 = $input->getArgument('arg1');

        if ($arg1) {
            $io->note(sprintf('You passed an argument: %s', $arg1));

            $val = $input->getArgument('arg2') ?? '';

            $client = $this->factory->create();
            $client->set($arg1, $val);

            $io->success('Set');
        }

        return Command::SUCCESS;
    }
}

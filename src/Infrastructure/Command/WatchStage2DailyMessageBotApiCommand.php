<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\UseCase\Daily;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'watch:stage-2-daily',
    description: 'Watching daily message from bot API',
)]
class WatchStage2DailyMessageBotApiCommand extends Command
{
    public function __construct(
        private readonly ?Daily $daily = null
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->daily->run();

        $io->success('Daily message.');

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\UseCase\Research;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'watch:stage-1-research',
    description: 'Watching and send questions to bot API',
)]
class WatchStage1ResearchMessageBotApiCommand extends Command
{
    public function __construct(
        private readonly Research $researchAction
    ) {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->researchAction->run();

        $io->success('Command `watch:stage-1-research` done.');

        return Command::SUCCESS;
    }
}

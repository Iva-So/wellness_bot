<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\UseCase\DataDailyMessageQueue;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'watch:data-daily-message-queue',
    description: 'Watching data daily message from queue',
)]
class WatchDataDailyMessageQueueCommand extends Command
{
    public function __construct(
        private readonly ?DataDailyMessageQueue $dailyMessageQueue = null
    ) {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->dailyMessageQueue->run();

        return Command::SUCCESS;
    }
}

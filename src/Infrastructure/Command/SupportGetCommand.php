<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'supp:get',
    description: 'Support: Get value by key',
)]
class SupportGetCommand extends Command
{
    private FactoryInterface $factory;

    public function __construct()
    {
        parent::__construct();

        $this->factory = new RedisFactory();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $arg = $input->getArgument('arg1') ?? '';
        $client = $this->factory->create();

        $val = $client->get($arg);

        $io->info(var_export($val, true));

        return Command::SUCCESS;
    }
}

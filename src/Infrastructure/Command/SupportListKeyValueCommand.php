<?php

declare(strict_types=1);

namespace App\Infrastructure\Command;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'supp:list-key-val',
    description: 'Support: List key -> value, default `*`',
)]
class SupportListKeyValueCommand extends Command
{
    private FactoryInterface $factory;

    public function __construct()
    {
        parent::__construct();

        $this->factory = new RedisFactory();
    }

    protected function configure(): void
    {
        $this->addArgument('pattern', InputArgument::OPTIONAL, 'pattern');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $pattern = $input->getArgument('pattern');

        $arg = $pattern ?? '*';
        $client = $this->factory->create();

        $keys = $client->keys($arg);
        echo PHP_EOL;
        foreach ($keys as $key) {
            $postFix = substr($key, -4);
            if (!in_array($postFix, ['done', 'wait'])) {
                $val = $client->get($key);
                echo sprintf('%42s -> %s', $key, $val);
                echo PHP_EOL;
            }
        }

        $io->success('Command `supp:list-key-val` done.');

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\ElasticSearch;

use App\Application\Constants\ElasticSearchConst;
use App\Application\Exceptions\ElasticSearchIndexCreateException;
use App\Application\Factory\ElasticIndexFactory;
use App\Application\Factory\FactoryInterface;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\MissingParameterException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'elastic:index-create',
    description: 'ElasticSearch: Create index',
)]
class CreateIndexCommand extends Command
{
    private ClientInterface $client;
    private FactoryInterface $factory;

    /**
     * @throws AuthenticationException
     */
    public function __construct()
    {
        parent::__construct();

        $this->factory = new ElasticIndexFactory();
        $this->client = $this->factory->create();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $indexName = $this->setIndex(ElasticSearchConst::ELASTIC_INDEX_NAME);

        try {
            $this->createIndex($indexName);
        } catch (ElasticSearchIndexCreateException $e) {
            echo $e->getMessage();

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    /**
     * @throws ElasticSearchIndexCreateException
     */
    private function createIndex(array $indexName): void
    {
        try {
            $this->client->indices()->create($indexName);
        } catch (ClientResponseException | MissingParameterException | ServerResponseException) {
            throw new ElasticSearchIndexCreateException();
        }
    }

    private function setIndex(string $indexName): array
    {
        return [
            'index' => $indexName,
            'body' => [
                'mappings' => [
                    'properties' => [
                        'name' => [
                            'type' => 'text',
                        ],
                        'calories' => [
                            'type' => 'long',
                        ],
                        'fat_total_g' => [
                            'type' => 'long'
                        ],
                        'protein_g' => [
                            'type' => 'long'
                        ],
                        'carbohydrates_total_g' => [
                            'type' => 'long'
                        ],
                        'serving_size_g' => [
                            'type' => 'long',
                        ],
                    ],
                ],
                'settings' => [
                    'analysis' => [
                        'filter' => [
                            'ru_stop' => [
                                'type' => 'stop',
                                'stopwords' => '_russian_',
                            ],
                            'ru_stemmer' => [
                                'type' => 'stemmer',
                                'language' => 'russian',
                            ],
                        ],
                        'analyzer' => [
                            'my_russian' => [
                                'tokenizer' => 'standard',
                                'filter' => [
                                    'lowercase',
                                    'ru_stop',
                                    'ru_stemmer',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}

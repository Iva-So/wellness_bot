<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\ElasticSearch;

use App\Application\Exceptions\ElasticSearchDocumentCreateException;
use App\Application\Factory\ElasticIndexFactory;
use App\Application\Factory\FactoryInterface;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\MissingParameterException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'elastic:document-create',
    description: 'ElasticSearch: Create document',
)]
class CreateDocumentCommand extends Command
{
    private ClientInterface $client;
    private FactoryInterface $factory;

    private const FOOD_DATA = __DIR__ . '/../../../Resources/elastic_data_bulk.json';

    /**
     * @throws AuthenticationException
     */
    public function __construct()
    {
        parent::__construct();

        $this->factory = new ElasticIndexFactory();
        $this->client = $this->factory->create();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->createDocument();
        } catch (ElasticSearchDocumentCreateException $e) {
            echo $e->getMessage();

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    /**
     * @throws ElasticSearchDocumentCreateException
     */
    private function createDocument(): void
    {
        $handle = fopen(self::FOOD_DATA, 'r');

        while (!feof($handle)) {
            $line = fgets($handle);

            if (!empty($line)) {
                $document = json_decode($line, true);

                if (isset($document['doc'])) {
                    $index = $document['doc']['_index'];
                    $indexArr = [
                        'index' => $index,
                    ];
                }

                if (isset($document['name'])) {
                    $body = ['body' => $document];
                    $param = array_merge($indexArr, $body);

                    try {
                        $this->client->index($param);
                    } catch (ClientResponseException | MissingParameterException | ServerResponseException) {
                        throw new ElasticSearchDocumentCreateException();
                    }
                }
            }
        }

        fclose($handle);
    }
}

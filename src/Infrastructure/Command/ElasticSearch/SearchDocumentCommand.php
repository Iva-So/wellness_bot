<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\ElasticSearch;
use App\Application\Service\ElasticSaveProductService;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'elastic:document-search',
    description: 'ElasticSearch: Search document',
)]
class SearchDocumentCommand extends Command
{
    public function __construct(private readonly ElasticSaveProductService $elasticSaveProductService)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('productInfo', InputArgument::REQUIRED, 'Product info')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $productFromElastic = $this->elasticSaveProductService->getProduct($input->getArgument('productInfo'));

        dd($productFromElastic);

        return Command::SUCCESS;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Command\ElasticSearch;

use App\Application\Constants\ElasticSearchConst;
use App\Application\Exceptions\ElasticSearchIndexDeleteException;
use App\Application\Factory\ElasticIndexFactory;
use App\Application\Factory\FactoryInterface;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\MissingParameterException;
use Elastic\Elasticsearch\Exception\ServerResponseException;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'elastic:index-delete',
    description: 'ElasticSearch: Delete index',
)]
class DeleteIndexCommand extends Command
{
    private ClientInterface $client;
    private FactoryInterface $factory;

    /**
     * @throws AuthenticationException
     */
    public function __construct()
    {
        parent::__construct();

        $this->factory = new ElasticIndexFactory();
        $this->client = $this->factory->create();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $this->deleteIndex(ElasticSearchConst::ELASTIC_INDEX_NAME);
        } catch (ElasticSearchIndexDeleteException $e) {
            echo $e->getMessage();

            return Command::FAILURE;
        }

        return Command::SUCCESS;
    }

    /**
     * @throws ElasticSearchIndexDeleteException
     */
    public function deleteIndex(string $indexName): void
    {
        try {
            $this->client->indices()->delete(['index' => $indexName]);
        } catch (ClientResponseException | MissingParameterException | ServerResponseException) {
            throw new ElasticSearchIndexDeleteException();
        }
    }
}

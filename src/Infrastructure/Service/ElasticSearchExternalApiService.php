<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use App\Application\Constants\ElasticSearchConst;
use App\Application\Exceptions\ElasticSearchExternalApiException;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

readonly class ElasticSearchExternalApiService implements ExternalApiServiceInterface
{
    public function __construct(
        private HttpClientInterface $httpClient
    ) {
    }

    /**
     * @throws ElasticSearchExternalApiException
     */
    public function getProductData(string $productName): ResponseInterface
    {
        try {
            $response = $this->httpClient->request(
                ElasticSearchConst::ELASTIC_EXTERNAL_API_METHOD,
                ElasticSearchConst::ELASTIC_EXTERNAL_API_URL . ElasticSearchConst::ELASTIC_PRODUCT_DEFAULT_WEIGHT . $productName, [
                'headers' => [
                    ElasticSearchConst::ELASTIC_EXTERNAL_API_HEADER => $_ENV['ELASTIC_EXTERNAL_API_KEY'],
                ],
            ]);
        } catch (TransportExceptionInterface $exception) {
            throw new ElasticSearchExternalApiException($exception->getMessage());
        }

        return $response;
    }
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Service;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

readonly class CreateAnswerControllerService implements CreateAnswerInterface
{
    private SerializerInterface $serializer;

    public function __construct()
    {
        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    public function createAnswer(object $objectDto): JsonResponse
    {
        $serializedProduct = $this->serializer->serialize($objectDto, 'json');

        return new JsonResponse([
            'message' => $serializedProduct,
            'status' => Response::HTTP_OK,
        ]);
    }
}

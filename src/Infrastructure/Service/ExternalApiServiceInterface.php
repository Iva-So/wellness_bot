<?php

namespace App\Infrastructure\Service;

use App\Application\Exceptions\ElasticSearchExternalApiException;
use Symfony\Contracts\HttpClient\ResponseInterface;

interface ExternalApiServiceInterface
{
    /**
     * @throws ElasticSearchExternalApiException
     */
    public function getProductData(string $productName): ResponseInterface;
}

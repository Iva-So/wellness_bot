<?php

namespace App\Infrastructure\Service;

use Symfony\Component\HttpFoundation\JsonResponse;

interface CreateAnswerInterface
{
    public function createAnswer(object $objectDto): JsonResponse;
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Log;

class FileLog implements LogInterface
{
    private string $pathToLog;

    public function __construct()
    {
        $fileLog = 'log.log';
        $this->pathToLog = implode(DIRECTORY_SEPARATOR, [
            dirname(__DIR__, 3),
            'var',
            'log',
            $fileLog,
        ]);
    }

    public function write(string $message, int $level = 0): void
    {
        $trace = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
        $calledClass = $trace[1]['class'] ?? 'unknown';
        $matchCompactPaths = [
            'App\Application\UseCase\SendMultipleChoiceQuestionMessageUpgrade' => 'A\A\UC\SendMultipleChoiceQuestionMessageUpgrade',
            'App\Application\UseCase\DoStage1SurveyUseCase' => 'A\A\UC\DoStage1SurveyUseCase',
            'App\Application\UseCase\TelegramGetUpdates' => 'A\A\UC\TelegramGetUpdates',
            'App\Infrastructure\Command\TelegramBotAPI\GetUpdatesQuery' => 'A\I\C\TBAPI\GetUpdatesQuery',
            'App\Infrastructure\Command\TelegramBotAPI\TelegramCronMockCommand' => 'A\I\C\TBAPI\TelegramCronMockCommand',
            'App\Application\Adapter\MatchMessageToUseCase' => 'A\A\A\MatchMessageToUseCase',
        ];

        $calledClass = $matchCompactPaths[$calledClass] ?? $calledClass;

        $preFix = match ($level) {
            LogInterface::LOG_INFO => 'INF',
            LogInterface::LOG_DEBUG => 'DBG',
            LogInterface::LOG_ERROR => 'ERR',
        };
        $compactFormat = 'ymd His';
        $message = sprintf(
            '[%s] %s %s: %s',
            date($compactFormat),
            $preFix,
            $calledClass,
            $message
        );
        error_log($message, 3, $this->pathToLog);
        error_log(PHP_EOL, 3, $this->pathToLog);
    }
}

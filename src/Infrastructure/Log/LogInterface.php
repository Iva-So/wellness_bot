<?php

declare(strict_types=1);

namespace App\Infrastructure\Log;

interface LogInterface
{
    final public const LOG_DEBUG = 0;
    final public const LOG_INFO = 1;
    final public const LOG_ERROR = 2;

    public function write(string $message, int $level = 0): void;
}

<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Contract\EntityInterface;
use App\Domain\Contract\WeightRepositoryInterface;
use App\Domain\Entity\Customer;
use App\Domain\Entity\Weight;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Weight>
 *
 * @method Weight|null find($id, $lockMode = null, $lockVersion = null)
 * @method Weight|null findOneBy(array $criteria, array $orderBy = null)
 * @method Weight[]    findAll()
 * @method Weight[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WeightRepository extends ServiceEntityRepository implements WeightRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Weight::class);
    }

    public function persist(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastWeightByCustomer(Customer $customer): ?Weight
    {
        $qb = $this->createQueryBuilder('w')
            ->where('w.customer = :customer')
            ->orderBy('w.createdAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('customer', $customer);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

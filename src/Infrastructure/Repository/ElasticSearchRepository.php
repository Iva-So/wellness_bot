<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Application\Constants\ElasticSearchConst;
use App\Application\Exceptions\ElasticSearchDocumentSaveException;
use App\Application\Factory\ElasticIndexFactory;
use App\Application\Factory\FactoryInterface;
use App\Domain\Contract\ElasticSearchRepositoryInterface;
use Elastic\Elasticsearch\ClientInterface;
use Elastic\Elasticsearch\Exception\AuthenticationException;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Elastic\Elasticsearch\Exception\MissingParameterException;
use Elastic\Elasticsearch\Exception\ServerResponseException;

class ElasticSearchRepository implements ElasticSearchRepositoryInterface
{
    private ClientInterface $client;
    private FactoryInterface $factory;

    /**
     * @throws AuthenticationException
     */
    public function __construct()
    {
        $this->factory = new ElasticIndexFactory();
        $this->client = $this->factory->create();
    }

    /**
     * @throws ElasticSearchDocumentSaveException
     */
    public function save(array $data): void
    {
        try {
            $document = [
                'index' => ElasticSearchConst::ELASTIC_INDEX_NAME,
                'body' => [
                    'name' => $data['name'],
                    'calories' => $data['calories'],
                    'fat_total_g' => $data['fat_total_g'],
                    'protein_g' => $data['protein_g'],
                    'carbohydrates_total_g' => $data['carbohydrates_total_g'],
                    'serving_size_g' => $data['serving_size_g'],
                ],
            ];

            $this->client->index($document);
        } catch (ServerResponseException|ClientResponseException|MissingParameterException $exception) {
            throw new ElasticSearchDocumentSaveException($exception);
        }
    }
}

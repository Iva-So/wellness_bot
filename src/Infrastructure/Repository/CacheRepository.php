<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use App\Domain\Contract\CacheRepositoryInterface;
use Predis\Client as RedisClient;

class CacheRepository implements CacheRepositoryInterface
{
    private FactoryInterface $factory;
    private RedisClient $client;

    public function __construct()
    {
        $this->factory = new RedisFactory();
        $this->client = $this->factory->create();
    }

    public function set(string $key, $value): void
    {
        $this->client->set($key, $value);
    }

    public function get(string $key): ?string
    {
        return $this->client->get($key);
    }

    public function delete(string $key): void
    {
        $this->client->del($key);
    }
}

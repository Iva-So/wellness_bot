<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Contract\EntityInterface;
use App\Domain\Contract\ProductRepositoryInterface;
use App\Domain\Entity\Customer;
use App\Domain\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\DBAL\Exception;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Product>
 *
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository implements ProductRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    public function persist(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    /**
     * @throws Exception
     */
    public function getSumDailyKBZHU(Customer $customer): array
    {
        $conn = $this->getEntityManager()->getConnection();
        $sql = "SELECT SUM(calorie)      AS daily_calorie,
                       SUM(protein)      AS protein,
                       SUM(fat)          AS fat,
                       SUM(carbohydrate) AS carbohydrate
                FROM product
                WHERE customer_id = :customerId
                  AND TO_CHAR(created_at, 'YYYY-MM-DD') = :today";

        $stmt = $conn->prepare($sql);
        $result = $stmt->executeQuery([
            'customerId' => $customer->getId(),
            'today' => date('Y-m-d'),
        ]);

        return $result->fetchAssociative();
    }
}

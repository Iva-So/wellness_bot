<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Application\Factory\FactoryInterface;
use App\Application\Factory\RedisFactory;
use App\Domain\Contract\HistoryRepositoryInterface;
use Predis\Client as RedisClient;

class HistoryRepository implements HistoryRepositoryInterface
{
    private FactoryInterface $factory;
    private RedisClient $client;

    public function __construct()
    {
        $this->factory = new RedisFactory();
        $this->client = $this->factory->create();
    }

    public function set(string $key, $value): void
    {
        $this->client->set($key, $value);
    }

    public function get(string $key): ?string
    {
        return $this->client->get($key);
    }

    public function delete(string $key): void
    {
        $this->client->del($key);
    }

    public function deleteAll(): void
    {
        $this->client->flushall();
    }

    public function add(string $chatId, string $questionId, string $answerText): void
    {
        $key = "user-chat:$chatId";
        $value = "question:$questionId,answer:$answerText";
        $this->client->lPush($key, [$value]);
    }

    public function count(string $chatId): int
    {
        return $this->client->lLen("user-chat:$chatId");
    }

    public function extractLast(string $chatId): string
    {
        $val = $this->client->lPop("user-chat:$chatId");

        return $val[0] ?? '';
    }

    public function extractFirst(string $chatId): string
    {
        $val = $this->client->rPop("user-chat:$chatId");

        return $val[0] ?? '';
    }

    public function extractAll(string $chatId): array
    {
        return $this->client->lRange("user-chat:$chatId", 0, -1);
    }
}

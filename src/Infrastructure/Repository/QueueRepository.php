<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Contract\QueueRepositoryInterface;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class QueueRepository implements QueueRepositoryInterface
{
    private AMQPStreamConnection $client;

    public function __construct()
    {
        $this->client = new AMQPStreamConnection(
            $_ENV['RABBITMQ_HOST'] ?? '127.0.0.1',
            $_ENV['RABBITMQ_PORT'] ?? '5672',
            $_ENV['RABBITMQ_USER'] ?? 'guest',
            $_ENV['RABBITMQ_PASSWORD'] ?? 'guest'
        );
    }

    public function publish(string $key, $value): void
    {
        $channel = $this->client->channel();
        $channel->queue_declare(
            $key,
            false,
            false,
            false,
            false
        );
        $exchange = '';
        $channel->basic_publish(
            new AMQPMessage($value),
            $exchange,
            $key
        );
    }

    public function subscribe(string $key, \Closure $callback): void
    {
        $channel = $this->client->channel();
        $channel->queue_declare(
            $key,
            false,
            false,
            false,
            false
        );

        $channel->basic_consume(
            $key,
            '',
            false,
            true,
            false,
            false,
            $callback
        );

        while (count($channel->callbacks)) {
            $channel->wait();
        }
    }
}

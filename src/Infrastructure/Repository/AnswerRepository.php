<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Contract\AnswerRepositoryInterface;
use App\Domain\Contract\EntityInterface;
use App\Domain\Entity\Answer;
use App\Domain\Entity\Customer;
use App\Domain\Entity\Question;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Answer>
 *
 * @method Answer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Answer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Answer[]    findAll()
 * @method Answer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnswerRepository extends ServiceEntityRepository implements AnswerRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Answer::class);
    }

    public function persist(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getLastAnswerByCustomerAndQuestion(Customer $customer, Question $question): Answer
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.customer = :customer')
            ->andWhere('a.question = :question')
            ->orderBy('a.createdAt', 'DESC')
            ->setMaxResults(1)
            ->setParameter('customer', $customer)
            ->setParameter('question', $question);

        return $qb->getQuery()->getSingleResult();
    }
}

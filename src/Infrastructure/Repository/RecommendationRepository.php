<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Contract\EntityInterface;
use App\Domain\Contract\RecommendationRepositoryInterface;
use App\Domain\Entity\Customer;
use App\Domain\Entity\Recommendation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Recommendation>
 *
 * @method Recommendation|null find($id, $lockMode = null, $lockVersion = null)
 * @method Recommendation|null findOneBy(array $criteria, array $orderBy = null)
 * @method Recommendation[]    findAll()
 * @method Recommendation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RecommendationRepository extends ServiceEntityRepository implements RecommendationRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recommendation::class);
    }

    public function persist(EntityInterface $entity): void
    {
        $this->_em->persist($entity);
    }

    public function flush(): void
    {
        $this->_em->flush();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getLastResearchRecommendation(Customer $customer): ?Recommendation
    {
        $qb = $this->createQueryBuilder('r')
            ->where('r.customer = :customer')
            ->setParameter('customer', $customer)
            ->orderBy('r.createdAt', 'DESC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }
}

version: '3.8'

services:
  ###> doctrine/doctrine-bundle ###
  database:
    container_name: wellness-db
    image: postgres:${POSTGRES_VERSION:-15}-alpine
    environment:
      POSTGRES_DB: ${POSTGRES_DB:-app}
      # You should definitely change the password in production
      POSTGRES_USER: ${POSTGRES_USER:-app}
      POSTGRES_PASSWORD: ${POSTGRES_PASSWORD:-app}
    ports:
      - ${APP_POSTGRES_PORT:-5433}:5432
    volumes:
      #      - database_data:/var/lib/postgresql/data:rw
      # You may use a bind-mounted host directory instead, so that it is harder to accidentally remove the volume and lose all your data!
      - ./docker/db/data:/var/lib/postgresql/data:rw
    ###< doctrine/doctrine-bundle ###
    deploy:
      resources:
        limits:
          memory: ${PG_MEMORY_LIMIT:-1G}
    networks:
      - app

  redis:
    container_name: wellness-redis
    build:
      context: docker/redis
      dockerfile: Dockerfile
    image: redis
    ports:
      - ${APP_REDIS_PORT:-6379}:${REDIS_PORT:-6379}
    volumes:
      - redis_data:/data
    deploy:
      resources:
        limits:
          memory: ${REDIS_MEMORY_LIMIT:-512M}
    networks:
      - app

  rabbitmq:
    container_name: wellness-rabbitmq
    build:
      context: docker/rabbitmq/
      dockerfile: Dockerfile
    image: rabbitmq
    ports:
      - ${APP_RABBITMQ_PORT:-5672}:${RABBITMQ_PORT:-5672}
      - ${APP_RABBITMQ_MANAGEMENT_PORT:-15672}:${RABBITMQ_MANAGEMENT_PORT:-15672}
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq
    deploy:
      resources:
        limits:
          memory: ${MQ_MEMORY_LIMIT:-512M}
    networks:
      - app

  php-cli:
    container_name: wellness-php-cli
    build:
      context: docker/php-cli
      dockerfile: Dockerfile
      target: ${APP_TARGET_MODE:-base}
    image: wellness-php-cli-image
    volumes:
      - ${APP_PATH_SRC:-.}:/srv/www
    deploy:
      resources:
        limits:
          memory: ${PHP_CLI_MEMORY_LIMIT:-512M}
    networks:
      - app

  php-cli-queue-daily:
    container_name: wellness-php-cli-queue-daily
    image: wellness-php-cli-image
    depends_on:
      - rabbitmq
    volumes:
      - ${APP_PATH_SRC:-.}:/srv/www
    deploy:
      restart_policy:
        condition: on-failure
      resources:
        limits:
          memory: ${PHP_QUEUE_MEMORY_LIMIT:-512M}
    command: /bin/sh -c "/srv/www/bin/console watch:data-daily-message-queue"
    networks:
      - app

  php-cli-queue-weekly:
    container_name: wellness-php-cli-queue-weekly
    image: wellness-php-cli-image
    depends_on:
      - rabbitmq
    volumes:
      - ${APP_PATH_SRC:-.}:/srv/www
    deploy:
      restart_policy:
        condition: on-failure
      resources:
        limits:
          memory: ${PHP_QUEUE_MEMORY_LIMIT:-512M}
    command: /bin/sh -c "/srv/www/bin/console watch:data-weekly-message-queue"
    networks:
      - app

  php-cli-cron:
    container_name: wellness-php-cli-cron
    image: wellness-php-cli-image
    depends_on:
      - rabbitmq
      - redis
      - elasticsearch
      - database
    volumes:
      - ${APP_PATH_SRC:-.}:/srv/www
    deploy:
      resources:
        limits:
          memory: ${PHP_CRON_MEMORY_LIMIT:-512M}
      restart_policy:
        condition: on-failure
    command: /bin/sh -c "while true; do /srv/www/bin/console telegram:cron-mock; sleep 4; done"
    networks:
      - app

  elasticsearch:
    image: elasticsearch:8.7.0
    container_name: wellness-elasticsearch
    restart: unless-stopped
    environment:
      - node.name=elastic
      - discovery.type=single-node
      - xpack.security.enabled=false
      - transport.host=localhost
    deploy:
      resources:
        limits:
          memory: ${ES_MEMORY_LIMIT:-1G}
    ports:
      - 9200:9200
    networks:
      - app

networks:
  app:

volumes:
  ###> doctrine/doctrine-bundle ###
  database_data:
  telegram-bot-api-data:
  rabbitmq_data:
  redis_data:
###< doctrine/doctrine-bundle ###

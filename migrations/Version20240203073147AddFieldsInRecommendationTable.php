<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240203073147AddFieldsInRecommendationTable extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recommendation ADD fat DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE recommendation ADD protein DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE recommendation ADD carbohydrate DOUBLE PRECISION NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE recommendation DROP fat');
        $this->addSql('ALTER TABLE recommendation DROP protein');
        $this->addSql('ALTER TABLE recommendation DROP carbohydrate');
    }
}

<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240202182252CreateRecommendationTable extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE recommendation_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE recommendation (id INT NOT NULL, customer_id INT NOT NULL, weight DOUBLE PRECISION NOT NULL, calorie DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_433224D29395C3F3 ON recommendation (customer_id)');
        $this->addSql('COMMENT ON COLUMN recommendation.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE recommendation ADD CONSTRAINT FK_433224D29395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE recommendation_id_seq CASCADE');
        $this->addSql('ALTER TABLE recommendation DROP CONSTRAINT FK_433224D29395C3F3');
        $this->addSql('DROP TABLE recommendation');
    }
}

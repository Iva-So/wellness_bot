<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240204072231CreateWeightTable extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SEQUENCE weight_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE weight (id INT NOT NULL, customer_id INT NOT NULL, current_weight DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_7CD55419395C3F3 ON weight (customer_id)');
        $this->addSql('COMMENT ON COLUMN weight.created_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE weight ADD CONSTRAINT FK_7CD55419395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP SEQUENCE weight_id_seq CASCADE');
        $this->addSql('ALTER TABLE weight DROP CONSTRAINT FK_7CD55419395C3F3');
        $this->addSql('DROP TABLE weight');
    }
}

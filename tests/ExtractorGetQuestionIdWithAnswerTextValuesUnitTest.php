<?php

namespace App\Tests;

use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class ExtractorGetQuestionIdWithAnswerTextValuesUnitTest extends TestCase
{
    public static function sourceDataProvider(): array
    {
        return [
            [
                "question:5,answer:>= 75",
                '5',
                '>= 75'
            ],
            [
                "question:53,answer:< 18",
                '53',
                '< 18'
            ],
            [
                "question:123,answer:на удачу",
                '123',
                'на удачу'
            ],
        ];
    }

    #[DataProvider('sourceDataProvider')]
    public function testAdd(string $sourceData, string $expectedQuestion, string $expectedAnswer): void
    {
        [$realQuestion, $realAnswer] = $this->theWayToExtractValues($sourceData);

        $this->assertSame($expectedQuestion, $realQuestion);
        $this->assertSame($expectedAnswer, $realAnswer);
    }

    private function theWayToExtractValues(string $data): array
    {
        preg_match("/question:(\d+),answer:(.*?)(?=,|$)/", $data, $matches);

        return [$matches[1], $matches[2]];
    }
}

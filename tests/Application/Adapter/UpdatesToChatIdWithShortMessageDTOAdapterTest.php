<?php

namespace App\Tests\Application\Adapter;

use App\Application\Adapter\UpdatesToChatIdWithShortMessageDTOAdapter;
use App\Application\DTO\ShortMessageDTO;
use ArrayIterator;
use PHPUnit\Framework\TestCase;
use SplFixedArray;

class UpdatesToChatIdWithShortMessageDTOAdapterTest extends TestCase
{
    public function testAdapt(): void
    {
        $subject = new UpdatesToChatIdWithShortMessageDTOAdapter();

        // Testing with an empty input
        $this->assertEquals([], $subject->adapt([]));

        // Testing with a single update
        $update = [
            'message' => [
                'chat' => ['id' => 123, 'first_name' => 'John'],
                'text' => 'Hello'
            ]
        ];
        $expectedResult = [123 => new ShortMessageDTO(123, 'Hello', 'John')];
        $this->assertEquals($expectedResult, $subject->adapt([$update]));

        // Testing with multiple updates
        $updates = [
            [
                'message' => [
                    'chat' => ['id' => 456, 'first_name' => 'Jane'],
                    'text' => 'Goodbye'
                ]
            ],
            [
                'message' => [
                    'chat' => ['id' => 789, 'first_name' => 'Bob'],
                    'text' => 'How are you?'
                ]
            ]
        ];
        $expectedResult = [
            456 => new ShortMessageDTO(456, 'Goodbye', 'Jane'),
            789 => new ShortMessageDTO(789, 'How are you?', 'Bob')
        ];
        $this->assertEquals($expectedResult, $subject->adapt($updates));
    }

    public function testAdaptMain(): void
    {
        $updates = [
            [
                'message' => [
                    'chat' => [
                        'id' => 1,
                        'first_name' => 'John'
                    ],
                    'text' => 'Hello, World!'
                ]
            ],

            [
                'message' => [
                    'chat' => [
                        'id' => 2,
                        'first_name' => 'Jane'
                    ],
                    'text' => 'Another message'
                ]
            ]
        ];

        $adapter = new UpdatesToChatIdWithShortMessageDTOAdapter();

        $chatIds = $adapter->adapt($updates);

        $this->assertCount(2, $chatIds);
        $this->assertArrayHasKey(1, $chatIds);
        $this->assertArrayHasKey(2, $chatIds);

        $this->assertInstanceOf(ShortMessageDTO::class, $chatIds[1]);
        $this->assertInstanceOf(ShortMessageDTO::class, $chatIds[2]);

        $this->assertEquals(1, $chatIds[1]->getUserId());
        $this->assertEquals('Hello, World!', $chatIds[1]->getMessageText());

        $this->assertEquals(2, $chatIds[2]->getUserId());

        $this->assertEquals('Another message', $chatIds[2]->getMessageText());
    }


    public function testAdaptToIterator(): void
    {
        $chatIds = [
            1 => new ShortMessageDTO(1, 'Hello, World!'),
            2 => new ShortMessageDTO(2, 'Another message')
        ];

        $adapter = new UpdatesToChatIdWithShortMessageDTOAdapter();

        $iterator = $adapter->adaptToIterator($chatIds);

        $this->assertInstanceOf(ArrayIterator::class, $iterator);
        $this->assertCount(2, $iterator);

        $iterator->rewind();

        $this->assertEquals(1, $iterator->key());
        $this->assertInstanceOf(ShortMessageDTO::class, $iterator->current());
        $this->assertEquals(1, $iterator->current()->getUserId());
        $this->assertEquals('Hello, World!', $iterator->current()->getMessageText());

        $iterator->next();

        $this->assertEquals(2, $iterator->key());
        $this->assertInstanceOf(ShortMessageDTO::class, $iterator->current());
        $this->assertEquals(2, $iterator->current()->getUserId());
        $this->assertEquals('Another message', $iterator->current()->getMessageText());
    }
}

<?php

namespace App\Tests\Application\Adapter;

use App\Application\Criteria\MessageCommandCriteria;
use App\Application\Enum\MenuCommand;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;

class MessageTextToUseCaseTypeTest  extends TestCase
{
    public static function sourceDataProvider(): array
    {
        $menuCasaes = array_map(static fn (MenuCommand $command) => $command->value, MenuCommand::cases());
        $data = array_map(static fn ($command) => ['text' => $command, 'type' => 'command'], $menuCasaes);

        $data[] = ['text' => 'привет', 'type' => 'message'];
        $data[] = ['text' => 'как дела?', 'type' => 'message'];
        $data[] = ['text' => 'apple,400', 'type' => 'message'];
        $data[] = ['text' => '> 17', 'type' => 'message'];
        $data[] = ['text' => '< 90', 'type' => 'message'];
        $data[] = ['text' => '= 42', 'type' => 'message'];
        $data[] = ['text' => '<= 42', 'type' => 'message'];
        $data[] = ['text' => '>= 42', 'type' => 'message'];
        $data[] = ['text' => '! 42', 'type' => 'message'];
        $data[] = ['text' => '~ 42', 'type' => 'message'];
        $data[] = ['text' => '? 42', 'type' => 'message'];
        $data[] = ['text' => '/', 'type' => 'message'];
        //['text' => '/faq', 'type' => 'default'],//not defined
        $data[] = ['text' => 'аааа что делать?', 'type' => 'message'];

        return $data;
    }

    #[DataProvider('sourceDataProvider')]
    public function testRun(string $text, string $type): void
    {
        $realType = $this->getMessageType($text);

        $this->assertSame($type, $realType);
        $this->assertTrue(true);
    }

    private function getMessageType(string $text): string
    {
        return $this->isMessageCommand($text) ? 'command' : 'message';
    }

    private function isMessageCommand(string $messageText): bool
    {
        return (new MessageCommandCriteria())->isCommand($messageText);
    }
}

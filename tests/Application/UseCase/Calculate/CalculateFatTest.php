<?php

declare(strict_types=1);

namespace Application\UseCase\Calculate;

use App\Application\UseCase\Calculate\CalculateNutrients\CalculateFat;
use PHPUnit\Framework\TestCase;

class CalculateFatTest extends TestCase
{
    public function testCalcReturnsCorrectValueForPositiveInput()
    {
        $fat = new CalculateFat();

        $dailyCalorie = 2000;
        $expectedFat = 66.7;
        $this->assertEquals($expectedFat, $fat->calc($dailyCalorie));
    }

    public function testCalcReturnsCorrectValueForZeroInput()
    {
        $fat = new CalculateFat();

        $dailyCalorie = 0;
        $expectedFat = 0.0;
        $this->assertEquals($expectedFat, $fat->calc($dailyCalorie));
    }
}

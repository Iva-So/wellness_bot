<?php

declare(strict_types=1);

namespace Application\UseCase\Calculate;

use App\Application\UseCase\Calculate\CalculateNutrients\CalculateCarbohydrate;
use PHPUnit\Framework\TestCase;

class CalculateCarbohydrateTest extends TestCase
{
    public function testCalc()
    {
        $carbohydrate = new CalculateCarbohydrate();

        $result = $carbohydrate->calc(2000);
        $this->assertEquals(200.0, $result);
    }
}

<?php

declare(strict_types=1);

namespace Application\UseCase\Calculate;

use App\Application\DTO\CustomerParamDto;
use App\Application\DTO\ValueObject\Age;
use App\Application\DTO\ValueObject\Gender;
use App\Application\DTO\ValueObject\Height;
use App\Application\DTO\ValueObject\Weight;
use App\Application\Enum\Gender as GenderEnum;
use App\Application\UseCase\Calculate\CalculateCustomerParam\CalculateCustomerParamDailyCalorie;
use PHPUnit\Framework\TestCase;

class CalculateCustomerParamDailyCalorieTest extends TestCase
{
    public function testCalcForFemale()
    {
        $dailyCalorie = new CalculateCustomerParamDailyCalorie;

        $dto = new CustomerParamDto();
        $dto->weight = new Weight((string)60);
        $dto->height = new Height((string)170);
        $dto->age = new Age((string)30);
        $dto->gender = new Gender(GenderEnum::FEMALE->value);

        $result = $dailyCalorie->calc($dto);
        $this->assertEquals(1351, $result);
    }

    public function testCalcForMale()
    {
        $dailyCalorie = new CalculateCustomerParamDailyCalorie;

        $dto = new CustomerParamDto();
        $dto->weight = new Weight((string)60);
        $dto->height = new Height((string)180);
        $dto->age = new Age((string)35);
        $dto->gender = new Gender(GenderEnum::MALE->value);

        $result = $dailyCalorie->calc($dto);
        $this->assertEquals(1555, $result);
    }
}

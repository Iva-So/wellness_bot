<?php

declare(strict_types=1);

namespace Application\UseCase\Calculate;

use App\Application\DTO\CustomerParamDto;
use App\Application\DTO\ValueObject\Gender;
use App\Application\DTO\ValueObject\Height;
use App\Application\Enum\Gender as GenderEnum;
use App\Application\UseCase\Calculate\CalculateCustomerParam\CalculateCustomerParamNormalWeight;
use PHPUnit\Framework\TestCase;

class CalculateCustomerParamNormalWeightTest extends TestCase
{
    public function testCalcReturnsCorrectValueForFemaleCustomer()
    {
        $normalWeight = new CalculateCustomerParamNormalWeight();

        $dto = new CustomerParamDto();
        $dto->gender = new Gender(GenderEnum::FEMALE->value);
        $dto->height = new Height((string)170);

        $result = $normalWeight->calc($dto);
        $this->assertEquals(63.0, $result);
    }

    public function testCalcReturnsCorrectValueForMaleCustomer()
    {
        $normalWeight = new CalculateCustomerParamNormalWeight();

        $dto = new CustomerParamDto();
        $dto->gender = new Gender(GenderEnum::MALE->value);
        $dto->height = new Height((string)180);

        $result = $normalWeight->calc($dto);
        $this->assertEquals(76.0, $result);
    }
}

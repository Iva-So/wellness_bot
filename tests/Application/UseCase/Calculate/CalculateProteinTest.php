<?php

declare(strict_types=1);

namespace Application\UseCase\Calculate;

use App\Application\UseCase\Calculate\CalculateNutrients\CalculateProtein;
use PHPUnit\Framework\TestCase;

class CalculateProteinTest extends TestCase
{
    public function testCalcWithZeroCalorie()
    {
        $protein = new CalculateProtein();

        $result = $protein->calc(0);
        $this->assertEquals(0, $result);
    }

    public function testCalcWithNonZeroCalorie()
    {
        $protein = new CalculateProtein();

        $result = $protein->calc(2000);
        $this->assertEquals(150, $result);
    }
}

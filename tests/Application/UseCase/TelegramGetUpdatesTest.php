<?php

namespace App\Tests\Application\UseCase;

use App\Application\Adapter\UpdatesToChatIdWithShortMessageDTOAdapter as UpdatesToChatIds;
use App\Application\DTO\ShortMessageDTO;
use App\Application\UseCase\TelegramGetUpdates;
use App\Application\Adapter\MatchMessageToUseCase as MatchMsgToUseCase;
use App\Infrastructure\Log\LogInterface as Log;
use PHPUnit\Framework\TestCase;
use App\Application\Adapter\PreviosUpdateIdPortAdapter as PreUpdateIdPort;
use App\Infrastructure\Command\TelegramBotAPI\GetUpdatesQuery as BotGetUpdates;

class TelegramGetUpdatesTest extends TestCase
{
    public function testRun(): void
    {
        $log = $this->createMock(Log::class);
        $messageToUseCase = $this->createMock(MatchMsgToUseCase::class);
        $fetchResult = $this->createMock(BotGetUpdates::class);
        $adapter = $this->createMock(UpdatesToChatIds::class);

        $preUpdateId = $this->createMock(PreUpdateIdPort::class);
        $preUpdateId->expects($this->once())
            ->method('getPreviousUpdateId')
            ->willReturn('101014');
        $preUpdateId->expects($this->once())
            ->method('setPreviousUpdateId')
            ->with(101015);

        $telegramGetUpdates = new TelegramGetUpdates(
            $log,
            $messageToUseCase,
            $fetchResult,
            $adapter,
            $preUpdateId
        );

        $sourceResult = [
            [
                'update_id' => 101015,
                'message' => [
                    'chat' => [
                        'id' => 123456789,
                        'first_name' => 'John',
                    ],
                    'text' => 'Hello, bot!',
                ],
            ],
        ];

        $lastUpdateId = '101014';
        $fetchResult->expects($this->once())
            ->method('getUpdates')
            ->with($lastUpdateId)
            ->willReturn($sourceResult);

        $expectShortMessageDTO = new ShortMessageDTO(123456789, 'Hello, bot!');
        $adapter->expects($this->once())
            ->method('adapt')
            ->with($sourceResult)
            ->willReturn([
                123456789 => $expectShortMessageDTO,
            ]);

        $messageToUseCase->expects($this->once())
            ->method('adaptAndRun')
            ->with($expectShortMessageDTO);

        $this->assertTrue($telegramGetUpdates->run());
    }
}

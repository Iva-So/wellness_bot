test:
	bin/phpunit

#lint:
#	phpcs --standard=PSR12 src/
#
#fix:
#	phpcbf --standard=PSR12 src/
#
#format:
#	phpcbf --standard=PSR12 src/
#	phpcs --standard=PSR12 src/

ks:
	bin/console supp:keys

dc-ks:
	docker compose exec php-cli bin/console supp:keys

lkval:
	bin/console supp:list-key-val

dc-lkval:
	docker compose exec php-cli bin/console supp:list-key-val


lvals:
	bin/console supp:list-vals

dc-lvals:
	docker compose exec php-cli bin/console supp:list-vals

dcupd:
	docker compose up -d

dc-cin:
	docker compose exec php-cli composer install


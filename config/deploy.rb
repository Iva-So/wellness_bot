require 'net/ssh'

if ARGV.length < 3
  puts "We need arguments"
  exit
end

server = ARGV[0]
username = ARGV[1]
password = ARGV[2]

puts server
puts username

Net::SSH.start(server, username, password: password, auth_methods: ['password']) do |ssh|
  puts 'Connected to the server'

  out = ssh.exec!('cd /home/demo/q && git log --oneline -1')
  puts out
  out = ssh.exec!('cd /home/demo/q && git pull')
  puts out
  out = ssh.exec!('cd /home/demo/q && git rev-parse --abbrev-ref HEAD')
  puts out
  out = ssh.exec!('cd /home/demo/q && git log --oneline -1')
  puts out
end
